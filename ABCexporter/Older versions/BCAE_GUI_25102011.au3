#Region
#AutoIt3Wrapper_OutFile=BCAE_GUI_NEW_25102011.exe
#EndRegion
Global Const $gui_event_close = -3
Global Const $gui_event_minimize = -4
Global Const $gui_event_restore = -5
Global Const $gui_event_maximize = -6
Global Const $gui_event_primarydown = -7
Global Const $gui_event_primaryup = -8
Global Const $gui_event_secondarydown = -9
Global Const $gui_event_secondaryup = -10
Global Const $gui_event_mousemove = -11
Global Const $gui_event_resized = -12
Global Const $gui_event_dropped = -13
Global Const $gui_rundefmsg = "GUI_RUNDEFMSG"
Global Const $gui_avistop = 0
Global Const $gui_avistart = 1
Global Const $gui_aviclose = 2
Global Const $gui_checked = 1
Global Const $gui_indeterminate = 2
Global Const $gui_unchecked = 4
Global Const $gui_dropaccepted = 8
Global Const $gui_nodropaccepted = 4096
Global Const $gui_acceptfiles = $gui_dropaccepted
Global Const $gui_show = 16
Global Const $gui_hide = 32
Global Const $gui_enable = 64
Global Const $gui_disable = 128
Global Const $gui_focus = 256
Global Const $gui_nofocus = 8192
Global Const $gui_defbutton = 512
Global Const $gui_expand = 1024
Global Const $gui_ontop = 2048
Global Const $gui_fontitalic = 2
Global Const $gui_fontunder = 4
Global Const $gui_fontstrike = 8
Global Const $gui_dockauto = 1
Global Const $gui_dockleft = 2
Global Const $gui_dockright = 4
Global Const $gui_dockhcenter = 8
Global Const $gui_docktop = 32
Global Const $gui_dockbottom = 64
Global Const $gui_dockvcenter = 128
Global Const $gui_dockwidth = 256
Global Const $gui_dockheight = 512
Global Const $gui_docksize = 768
Global Const $gui_dockmenubar = 544
Global Const $gui_dockstatebar = 576
Global Const $gui_dockall = 802
Global Const $gui_dockborders = 102
Global Const $gui_gr_close = 1
Global Const $gui_gr_line = 2
Global Const $gui_gr_bezier = 4
Global Const $gui_gr_move = 6
Global Const $gui_gr_color = 8
Global Const $gui_gr_rect = 10
Global Const $gui_gr_ellipse = 12
Global Const $gui_gr_pie = 14
Global Const $gui_gr_dot = 16
Global Const $gui_gr_pixel = 18
Global Const $gui_gr_hint = 20
Global Const $gui_gr_refresh = 22
Global Const $gui_gr_pensize = 24
Global Const $gui_gr_nobkcolor = -2
Global Const $gui_bkcolor_default = -1
Global Const $gui_bkcolor_transparent = -2
Global Const $gui_bkcolor_lv_alternate = -33554432
Global Const $gui_ws_ex_parentdrag = 1048576
Global Const $bs_groupbox = 7
Global Const $bs_bottom = 2048
Global Const $bs_center = 768
Global Const $bs_defpushbutton = 1
Global Const $bs_left = 256
Global Const $bs_multiline = 8192
Global Const $bs_pushbox = 10
Global Const $bs_pushlike = 4096
Global Const $bs_right = 512
Global Const $bs_rightbutton = 32
Global Const $bs_top = 1024
Global Const $bs_vcenter = 3072
Global Const $bs_flat = 32768
Global Const $bs_icon = 64
Global Const $bs_bitmap = 128
Global Const $bs_notify = 16384
Global Const $bs_splitbutton = 12
Global Const $bs_defsplitbutton = 13
Global Const $bs_commandlink = 14
Global Const $bs_defcommandlink = 15
Global Const $bcsif_glyph = 1
Global Const $bcsif_image = 2
Global Const $bcsif_style = 4
Global Const $bcsif_size = 8
Global Const $bcss_nosplit = 1
Global Const $bcss_stretch = 2
Global Const $bcss_alignleft = 4
Global Const $bcss_image = 8
Global Const $button_imagelist_align_left = 0
Global Const $button_imagelist_align_right = 1
Global Const $button_imagelist_align_top = 2
Global Const $button_imagelist_align_bottom = 3
Global Const $button_imagelist_align_center = 4
Global Const $bs_3state = 5
Global Const $bs_auto3state = 6
Global Const $bs_autocheckbox = 3
Global Const $bs_checkbox = 2
Global Const $bs_radiobutton = 4
Global Const $bs_autoradiobutton = 9
Global Const $bs_ownerdraw = 11
Global Const $gui_ss_default_button = 0
Global Const $gui_ss_default_checkbox = 0
Global Const $gui_ss_default_group = 0
Global Const $gui_ss_default_radio = 0
Global Const $bcm_first = 5632
Global Const $bcm_getidealsize = ($bcm_first + 1)
Global Const $bcm_getimagelist = ($bcm_first + 3)
Global Const $bcm_getnote = ($bcm_first + 10)
Global Const $bcm_getnotelength = ($bcm_first + 11)
Global Const $bcm_getsplitinfo = ($bcm_first + 8)
Global Const $bcm_gettextmargin = ($bcm_first + 5)
Global Const $bcm_setdropdownstate = ($bcm_first + 6)
Global Const $bcm_setimagelist = ($bcm_first + 2)
Global Const $bcm_setnote = ($bcm_first + 9)
Global Const $bcm_setshield = ($bcm_first + 12)
Global Const $bcm_setsplitinfo = ($bcm_first + 7)
Global Const $bcm_settextmargin = ($bcm_first + 4)
Global Const $bm_click = 245
Global Const $bm_getcheck = 240
Global Const $bm_getimage = 246
Global Const $bm_getstate = 242
Global Const $bm_setcheck = 241
Global Const $bm_setdontclick = 248
Global Const $bm_setimage = 247
Global Const $bm_setstate = 243
Global Const $bm_setstyle = 244
Global Const $bcn_first = -1250
Global Const $bcn_dropdown = ($bcn_first + 2)
Global Const $bcn_hotitemchange = ($bcn_first + 1)
Global Const $bn_clicked = 0
Global Const $bn_paint = 1
Global Const $bn_hilite = 2
Global Const $bn_unhilite = 3
Global Const $bn_disable = 4
Global Const $bn_doubleclicked = 5
Global Const $bn_setfocus = 6
Global Const $bn_killfocus = 7
Global Const $bn_pushed = $bn_hilite
Global Const $bn_unpushed = $bn_unhilite
Global Const $bn_dblclk = $bn_doubleclicked
Global Const $bst_checked = 1
Global Const $bst_indeterminate = 2
Global Const $bst_unchecked = 0
Global Const $bst_focus = 8
Global Const $bst_pushed = 4
Global Const $bst_dontclick = 128
Global Const $ss_left = 0
Global Const $ss_center = 1
Global Const $ss_right = 2
Global Const $ss_icon = 3
Global Const $ss_blackrect = 4
Global Const $ss_grayrect = 5
Global Const $ss_whiterect = 6
Global Const $ss_blackframe = 7
Global Const $ss_grayframe = 8
Global Const $ss_whiteframe = 9
Global Const $ss_simple = 11
Global Const $ss_leftnowordwrap = 12
Global Const $ss_bitmap = 14
Global Const $ss_etchedhorz = 16
Global Const $ss_etchedvert = 17
Global Const $ss_etchedframe = 18
Global Const $ss_noprefix = 128
Global Const $ss_notify = 256
Global Const $ss_centerimage = 512
Global Const $ss_rightjust = 1024
Global Const $ss_sunken = 4096
Global Const $gui_ss_default_label = 0
Global Const $gui_ss_default_graphic = 0
Global Const $gui_ss_default_icon = $ss_notify
Global Const $gui_ss_default_pic = $ss_notify
Global Const $fw_dontcare = 0
Global Const $fw_thin = 100
Global Const $fw_extralight = 200
Global Const $fw_ultralight = 200
Global Const $fw_light = 300
Global Const $fw_normal = 400
Global Const $fw_regular = 400
Global Const $fw_medium = 500
Global Const $fw_semibold = 600
Global Const $fw_demibold = 600
Global Const $fw_bold = 700
Global Const $fw_extrabold = 800
Global Const $fw_ultrabold = 800
Global Const $fw_heavy = 900
Global Const $fw_black = 900
Global Const $cf_effects = 256
Global Const $cf_printerfonts = 2
Global Const $cf_screenfonts = 1
Global Const $cf_noscriptsel = 8388608
Global Const $cf_inittologfontstruct = 64
Global Const $logpixelsx = 88
Global Const $logpixelsy = 90
Global Const $ansi_charset = 0
Global Const $baltic_charset = 186
Global Const $chinesebig5_charset = 136
Global Const $default_charset = 1
Global Const $easteurope_charset = 238
Global Const $gb2312_charset = 134
Global Const $greek_charset = 161
Global Const $hangeul_charset = 129
Global Const $mac_charset = 77
Global Const $oem_charset = 255
Global Const $russian_charset = 204
Global Const $shiftjis_charset = 128
Global Const $symbol_charset = 2
Global Const $turkish_charset = 162
Global Const $vietnamese_charset = 163
Global Const $out_character_precis = 2
Global Const $out_default_precis = 0
Global Const $out_device_precis = 5
Global Const $out_outline_precis = 8
Global Const $out_ps_only_precis = 10
Global Const $out_raster_precis = 6
Global Const $out_string_precis = 1
Global Const $out_stroke_precis = 3
Global Const $out_tt_only_precis = 7
Global Const $out_tt_precis = 4
Global Const $clip_character_precis = 1
Global Const $clip_default_precis = 0
Global Const $clip_embedded = 128
Global Const $clip_lh_angles = 16
Global Const $clip_mask = 15
Global Const $clip_stroke_precis = 2
Global Const $clip_tt_always = 32
Global Const $antialiased_quality = 4
Global Const $default_quality = 0
Global Const $draft_quality = 1
Global Const $nonantialiased_quality = 3
Global Const $proof_quality = 2
Global Const $default_pitch = 0
Global Const $fixed_pitch = 1
Global Const $variable_pitch = 2
Global Const $ff_decorative = 80
Global Const $ff_dontcare = 0
Global Const $ff_modern = 48
Global Const $ff_roman = 16
Global Const $ff_script = 64
Global Const $ff_swiss = 32
Global Const $tagpoint = "long X;long Y"
Global Const $tagrect = "long Left;long Top;long Right;long Bottom"
Global Const $tagsize = "long X;long Y"
Global Const $tagmargins = "int cxLeftWidth;int cxRightWidth;int cyTopHeight;int cyBottomHeight"
Global Const $tagfiletime = "dword Lo;dword Hi"
Global Const $tagsystemtime = "word Year;word Month;word Dow;word Day;word Hour;word Minute;word Second;word MSeconds"
Global Const $tagtime_zone_information = "long Bias;wchar StdName[32];word StdDate[8];long StdBias;wchar DayName[32];word DayDate[8];long DayBias"
Global Const $tagnmhdr = "hwnd hWndFrom;uint_ptr IDFrom;INT Code"
Global Const $tagcomboboxexitem = "uint Mask;int_ptr Item;ptr Text;int TextMax;int Image;int SelectedImage;int OverlayImage;" & "int Indent;lparam Param"
Global Const $tagnmcbedragbegin = $tagnmhdr & ";int ItemID;ptr szText"
Global Const $tagnmcbeendedit = $tagnmhdr & ";bool fChanged;int NewSelection;ptr szText;int Why"
Global Const $tagnmcomboboxex = $tagnmhdr & ";uint Mask;int_ptr Item;ptr Text;int TextMax;int Image;" & "int SelectedImage;int OverlayImage;int Indent;lparam Param"
Global Const $tagdtprange = "word MinYear;word MinMonth;word MinDOW;word MinDay;word MinHour;word MinMinute;" & "word MinSecond;word MinMSecond;word MaxYear;word MaxMonth;word MaxDOW;word MaxDay;word MaxHour;" & "word MaxMinute;word MaxSecond;word MaxMSecond;bool MinValid;bool MaxValid"
Global Const $tagnmdatetimechange = $tagnmhdr & ";dword Flag;" & $tagsystemtime
Global Const $tagnmdatetimeformat = $tagnmhdr & ";ptr Format;" & $tagsystemtime & ";ptr pDisplay;wchar Display[64]"
Global Const $tagnmdatetimeformatquery = $tagnmhdr & ";ptr Format;long SizeX;long SizeY"
Global Const $tagnmdatetimekeydown = $tagnmhdr & ";int VirtKey;ptr Format;" & $tagsystemtime
Global Const $tagnmdatetimestring = $tagnmhdr & ";ptr UserString;" & $tagsystemtime & ";dword Flags"
Global Const $tageventlogrecord = "dword Length;dword Reserved;dword RecordNumber;dword TimeGenerated;dword TimeWritten;dword EventID;" & "word EventType;word NumStrings;word EventCategory;word ReservedFlags;dword ClosingRecordNumber;dword StringOffset;" & "dword UserSidLength;dword UserSidOffset;dword DataLength;dword DataOffset"
Global Const $taggdipbitmapdata = "uint Width;uint Height;int Stride;int Format;ptr Scan0;uint_ptr Reserved"
Global Const $taggdipencoderparam = "byte GUID[16];dword Count;dword Type;ptr Values"
Global Const $taggdipencoderparams = "dword Count;byte Params[0]"
Global Const $taggdiprectf = "float X;float Y;float Width;float Height"
Global Const $taggdipstartupinput = "uint Version;ptr Callback;bool NoThread;bool NoCodecs"
Global Const $taggdipstartupoutput = "ptr HookProc;ptr UnhookProc"
Global Const $taggdipimagecodecinfo = "byte CLSID[16];byte FormatID[16];ptr CodecName;ptr DllName;ptr FormatDesc;ptr FileExt;" & "ptr MimeType;dword Flags;dword Version;dword SigCount;dword SigSize;ptr SigPattern;ptr SigMask"
Global Const $taggdippencoderparams = "dword Count;byte Params[0]"
Global Const $taghditem = "uint Mask;int XY;ptr Text;handle hBMP;int TextMax;int Fmt;lparam Param;int Image;int Order;uint Type;ptr pFilter;uint State"
Global Const $tagnmhddispinfo = $tagnmhdr & ";int Item;uint Mask;ptr Text;int TextMax;int Image;lparam lParam"
Global Const $tagnmhdfilterbtnclick = $tagnmhdr & ";int Item;" & $tagrect
Global Const $tagnmheader = $tagnmhdr & ";int Item;int Button;ptr pItem"
Global Const $taggetipaddress = "byte Field4;byte Field3;byte Field2;byte Field1"
Global Const $tagnmipaddress = $tagnmhdr & ";int Field;int Value"
Global Const $taglvfindinfo = "uint Flags;ptr Text;lparam Param;" & $tagpoint & ";uint Direction"
Global Const $taglvhittestinfo = $tagpoint & ";uint Flags;int Item;int SubItem"
Global Const $taglvitem = "uint Mask;int Item;int SubItem;uint State;uint StateMask;ptr Text;int TextMax;int Image;lparam Param;" & "int Indent;int GroupID;uint Columns;ptr pColumns"
Global Const $tagnmlistview = $tagnmhdr & ";int Item;int SubItem;uint NewState;uint OldState;uint Changed;" & "long ActionX;long ActionY;lparam Param"
Global Const $tagnmlvcustomdraw = $tagnmhdr & ";dword dwDrawStage;handle hdc;long Left;long Top;long Right;long Bottom;" & "dword_ptr dwItemSpec;uint uItemState;lparam lItemlParam" & ";dword clrText;dword clrTextBk;int iSubItem;dword dwItemType;dword clrFace;int iIconEffect;" & "int iIconPhase;int iPartId;int iStateId;long TextLeft;long TextTop;long TextRight;long TextBottom;uint uAlign"
Global Const $tagnmlvdispinfo = $tagnmhdr & ";" & $taglvitem
Global Const $tagnmlvfinditem = $tagnmhdr & ";" & $taglvfindinfo
Global Const $tagnmlvgetinfotip = $tagnmhdr & ";dword Flags;ptr Text;int TextMax;int Item;int SubItem;lparam lParam"
Global Const $tagnmitemactivate = $tagnmhdr & ";int Index;int SubItem;uint NewState;uint OldState;uint Changed;" & $tagpoint & ";lparam lParam;uint KeyFlags"
Global Const $tagnmlvkeydown = $tagnmhdr & ";align 1;word VKey;uint Flags"
Global Const $tagnmlvscroll = $tagnmhdr & ";int DX;int DY"
Global Const $tagmchittestinfo = "uint Size;" & $tagpoint & ";uint Hit;" & $tagsystemtime
Global Const $tagmcmonthrange = "word MinYear;word MinMonth;word MinDOW;word MinDay;word MinHour;word MinMinute;word MinSecond;" & "word MinMSeconds;word MaxYear;word MaxMonth;word MaxDOW;word MaxDay;word MaxHour;word MaxMinute;word MaxSecond;" & "word MaxMSeconds;short Span"
Global Const $tagmcrange = "word MinYear;word MinMonth;word MinDOW;word MinDay;word MinHour;word MinMinute;word MinSecond;" & "word MinMSeconds;word MaxYear;word MaxMonth;word MaxDOW;word MaxDay;word MaxHour;word MaxMinute;word MaxSecond;" & "word MaxMSeconds;short MinSet;short MaxSet"
Global Const $tagmcselrange = "word MinYear;word MinMonth;word MinDOW;word MinDay;word MinHour;word MinMinute;word MinSecond;" & "word MinMSeconds;word MaxYear;word MaxMonth;word MaxDOW;word MaxDay;word MaxHour;word MaxMinute;word MaxSecond;" & "word MaxMSeconds"
Global Const $tagnmdaystate = $tagnmhdr & ";" & $tagsystemtime & ";int DayState;ptr pDayState"
Global Const $tagnmselchange = $tagnmhdr & ";word BegYear;word BegMonth;word BegDOW;word BegDay;" & "word BegHour;word BegMinute;word BegSecond;word BegMSeconds;word EndYear;word EndMonth;word EndDOW;" & "word EndDay;word EndHour;word EndMinute;word EndSecond;word EndMSeconds"
Global Const $tagnmobjectnotify = $tagnmhdr & ";int Item;ptr piid;ptr pObject;long Result"
Global Const $tagnmtckeydown = $tagnmhdr & ";word VKey;uint Flags"
Global Const $tagtvitem = "uint Mask;handle hItem;uint State;uint StateMask;ptr Text;int TextMax;int Image;int SelectedImage;" & "int Children;lparam Param"
Global Const $tagtvitemex = $tagtvitem & ";int Integral"
Global Const $tagnmtreeview = $tagnmhdr & ";uint Action;uint OldMask;handle OldhItem;uint OldState;uint OldStateMask;" & "ptr OldText;int OldTextMax;int OldImage;int OldSelectedImage;int OldChildren;lparam OldParam;uint NewMask;handle NewhItem;" & "uint NewState;uint NewStateMask;ptr NewText;int NewTextMax;int NewImage;int NewSelectedImage;int NewChildren;" & "lparam NewParam;long PointX;long PointY"
Global Const $tagnmtvcustomdraw = $tagnmhdr & ";dword DrawStage;handle HDC;long Left;long Top;long Right;long Bottom;" & "dword_ptr ItemSpec;uint ItemState;lparam ItemParam;dword ClrText;dword ClrTextBk;int Level"
Global Const $tagnmtvdispinfo = $tagnmhdr & ";" & $tagtvitem
Global Const $tagnmtvgetinfotip = $tagnmhdr & ";ptr Text;int TextMax;handle hItem;lparam lParam"
Global Const $tagtvhittestinfo = $tagpoint & ";uint Flags;handle Item"
Global Const $tagnmtvkeydown = $tagnmhdr & ";word VKey;uint Flags"
Global Const $tagnmmouse = $tagnmhdr & ";dword_ptr ItemSpec;dword_ptr ItemData;" & $tagpoint & ";lparam HitInfo"
Global Const $tagtoken_privileges = "dword Count;int64 LUID;dword Attributes"
Global Const $tagimageinfo = "handle hBitmap;handle hMask;int Unused1;int Unused2;" & $tagrect
Global Const $tagmenuinfo = "dword Size;INT Mask;dword Style;uint YMax;handle hBack;dword ContextHelpID;ulong_ptr MenuData"
Global Const $tagmenuiteminfo = "uint Size;uint Mask;uint Type;uint State;uint ID;handle SubMenu;handle BmpChecked;handle BmpUnchecked;" & "ulong_ptr ItemData;ptr TypeData;uint CCH;handle BmpItem"
Global Const $tagrebarbandinfo = "uint cbSize;uint fMask;uint fStyle;dword clrFore;dword clrBack;ptr lpText;uint cch;" & "int iImage;hwnd hwndChild;uint cxMinChild;uint cyMinChild;uint cx;handle hbmBack;uint wID;uint cyChild;uint cyMaxChild;" & "uint cyIntegral;uint cxIdeal;lparam lParam;uint cxHeader"
Global Const $tagnmrebarautobreak = $tagnmhdr & ";uint uBand;uint wID;lparam lParam;uint uMsg;uint fStyleCurrent;bool fAutoBreak"
Global Const $tagnmrbautosize = $tagnmhdr & ";bool fChanged;long TargetLeft;long TargetTop;long TargetRight;long TargetBottom;" & "long ActualLeft;long ActualTop;long ActualRight;long ActualBottom"
Global Const $tagnmrebar = $tagnmhdr & ";dword dwMask;uint uBand;uint fStyle;uint wID;laram lParam"
Global Const $tagnmrebarchevron = $tagnmhdr & ";uint uBand;uint wID;lparam lParam;" & $tagrect & ";lparam lParamNM"
Global Const $tagnmrebarchildsize = $tagnmhdr & ";uint uBand;uint wID;long CLeft;long CTop;long CRight;long CBottom;" & "long BLeft;long BTop;long BRight;long BBottom"
Global Const $tagcolorscheme = "dword Size;dword BtnHighlight;dword BtnShadow"
Global Const $tagnmtoolbar = $tagnmhdr & ";int iItem;" & "int iBitmap;int idCommand;byte fsState;byte fsStyle;align;dword_ptr dwData;int_ptr iString" & ";int cchText;ptr pszText;" & $tagrect
Global Const $tagnmtbhotitem = $tagnmhdr & ";int idOld;int idNew;dword dwFlags"
Global Const $tagtbbutton = "int Bitmap;int Command;byte State;byte Style;align;dword_ptr Param;int_ptr String"
Global Const $tagtbbuttoninfo = "uint Size;dword Mask;int Command;int Image;byte State;byte Style;word CX;dword_ptr Param;ptr Text;int TextMax"
Global Const $tagnetresource = "dword Scope;dword Type;dword DisplayType;dword Usage;ptr LocalName;ptr RemoteName;ptr Comment;ptr Provider"
Global Const $tagoverlapped = "ulong_ptr Internal;ulong_ptr InternalHigh;dword Offset;dword OffsetHigh;handle hEvent"
Global Const $tagopenfilename = "dword StructSize;hwnd hwndOwner;handle hInstance;ptr lpstrFilter;ptr lpstrCustomFilter;" & "dword nMaxCustFilter;dword nFilterIndex;ptr lpstrFile;dword nMaxFile;ptr lpstrFileTitle;dword nMaxFileTitle;" & "ptr lpstrInitialDir;ptr lpstrTitle;dword Flags;word nFileOffset;word nFileExtension;ptr lpstrDefExt;lparam lCustData;" & "ptr lpfnHook;ptr lpTemplateName;ptr pvReserved;dword dwReserved;dword FlagsEx"
Global Const $tagbitmapinfo = "dword Size;long Width;long Height;word Planes;word BitCount;dword Compression;dword SizeImage;" & "long XPelsPerMeter;long YPelsPerMeter;dword ClrUsed;dword ClrImportant;dword RGBQuad"
Global Const $tagblendfunction = "byte Op;byte Flags;byte Alpha;byte Format"
Global Const $tagguid = "dword Data1;word Data2;word Data3;byte Data4[8]"
Global Const $tagwindowplacement = "uint length; uint flags;uint showCmd;long ptMinPosition[2];long ptMaxPosition[2];long rcNormalPosition[4]"
Global Const $tagwindowpos = "hwnd hWnd;hwnd InsertAfter;int X;int Y;int CX;int CY;uint Flags"
Global Const $tagscrollinfo = "uint cbSize;uint fMask;int  nMin;int  nMax;uint nPage;int  nPos;int  nTrackPos"
Global Const $tagscrollbarinfo = "dword cbSize;" & $tagrect & ";int dxyLineButton;int xyThumbTop;" & "int xyThumbBottom;int reserved;dword rgstate[6]"
Global Const $taglogfont = "long Height;long Width;long Escapement;long Orientation;long Weight;byte Italic;byte Underline;" & "byte Strikeout;byte CharSet;byte OutPrecision;byte ClipPrecision;byte Quality;byte PitchAndFamily;wchar FaceName[32]"
Global Const $tagkbdllhookstruct = "dword vkCode;dword scanCode;dword flags;dword time;ulong_ptr dwExtraInfo"
Global Const $tagprocess_information = "handle hProcess;handle hThread;dword ProcessID;dword ThreadID"
Global Const $tagstartupinfo = "dword Size;ptr Reserved1;ptr Desktop;ptr Title;dword X;dword Y;dword XSize;dword YSize;dword XCountChars;" & "dword YCountChars;dword FillAttribute;dword Flags;word ShowWindow;word Reserved2;ptr Reserved3;handle StdInput;" & "handle StdOutput;handle StdError"
Global Const $tagsecurity_attributes = "dword Length;ptr Descriptor;bool InheritHandle"
Global Const $tagwin32_find_data = "dword dwFileAttributes; dword ftCreationTime[2]; dword ftLastAccessTime[2]; dword ftLastWriteTime[2]; dword nFileSizeHigh; dword nFileSizeLow; dword dwReserved0; dword dwReserved1; wchar cFileName[260]; wchar cAlternateFileName[14]"

Func _winapi_getlasterror($curerr = @error, $curext = @extended)
	Local $aresult = DllCall("kernel32.dll", "dword", "GetLastError")
	Return SetError($curerr, $curext, $aresult[0])
EndFunc

Func _winapi_setlasterror($ierrcode, $curerr = @error, $curext = @extended)
	DllCall("kernel32.dll", "none", "SetLastError", "dword", $ierrcode)
	Return SetError($curerr, $curext)
EndFunc

Global Const $__miscconstant_cc_anycolor = 256
Global Const $__miscconstant_cc_fullopen = 2
Global Const $__miscconstant_cc_rgbinit = 1
Global Const $tagchoosecolor = "dword Size;hwnd hWndOwnder;handle hInstance;dword rgbResult;ptr CustColors;dword Flags;lparam lCustData;" & "ptr lpfnHook;ptr lpTemplateName"
Global Const $tagchoosefont = "dword Size;hwnd hWndOwner;handle hDC;ptr LogFont;int PointSize;dword Flags;dword rgbColors;lparam CustData;" & "ptr fnHook;ptr TemplateName;handle hInstance;ptr szStyle;word FontType;int SizeMin;int SizeMax"

Func _choosecolor($ireturntype = 0, $icolorref = 0, $ireftype = 0, $hwndownder = 0)
	Local $custcolors = "dword[16]"
	Local $tchoose = DllStructCreate($tagchoosecolor)
	Local $tcc = DllStructCreate($custcolors)
	If $ireftype = 1 Then
		$icolorref = Int($icolorref)
	ElseIf $ireftype = 2 Then
		$icolorref = Hex(String($icolorref), 6)
		$icolorref = "0x" & StringMid($icolorref, 5, 2) & StringMid($icolorref, 3, 2) & StringMid($icolorref, 1, 2)
	EndIf
	DllStructSetData($tchoose, "Size", DllStructGetSize($tchoose))
	DllStructSetData($tchoose, "hWndOwnder", $hwndownder)
	DllStructSetData($tchoose, "rgbResult", $icolorref)
	DllStructSetData($tchoose, "CustColors", DllStructGetPtr($tcc))
	DllStructSetData($tchoose, "Flags", BitOR($__miscconstant_cc_anycolor, $__miscconstant_cc_fullopen, $__miscconstant_cc_rgbinit))
	Local $aresult = DllCall("comdlg32.dll", "bool", "ChooseColor", "ptr", DllStructGetPtr($tchoose))
	If @error Then Return SetError(@error, @extended, -1)
	If $aresult[0] = 0 Then Return SetError(-3, -3, -1)
	Local $color_picked = DllStructGetData($tchoose, "rgbResult")
	If $ireturntype = 1 Then
		Return "0x" & Hex(String($color_picked), 6)
	ElseIf $ireturntype = 2 Then
		$color_picked = Hex(String($color_picked), 6)
		Return "0x" & StringMid($color_picked, 5, 2) & StringMid($color_picked, 3, 2) & StringMid($color_picked, 1, 2)
	ElseIf $ireturntype = 0 Then
		Return $color_picked
	Else
		Return SetError(-4, -4, -1)
	EndIf
EndFunc

Func _choosefont($sfontname = "Courier New", $ipointsize = 10, $icolorref = 0, $ifontweight = 0, $iitalic = False, $iunderline = False, $istrikethru = False, $hwndowner = 0)
	Local $italic = 0, $underline = 0, $strikeout = 0
	Local $lngdc = __misc_getdc(0)
	Local $lfheight = Round(($ipointsize * __misc_getdevicecaps($lngdc, $logpixelsx)) / 72, 0)
	__misc_releasedc(0, $lngdc)
	Local $tchoosefont = DllStructCreate($tagchoosefont)
	Local $tlogfont = DllStructCreate($taglogfont)
	DllStructSetData($tchoosefont, "Size", DllStructGetSize($tchoosefont))
	DllStructSetData($tchoosefont, "hWndOwner", $hwndowner)
	DllStructSetData($tchoosefont, "LogFont", DllStructGetPtr($tlogfont))
	DllStructSetData($tchoosefont, "PointSize", $ipointsize)
	DllStructSetData($tchoosefont, "Flags", BitOR($cf_screenfonts, $cf_printerfonts, $cf_effects, $cf_inittologfontstruct, $cf_noscriptsel))
	DllStructSetData($tchoosefont, "rgbColors", $icolorref)
	DllStructSetData($tchoosefont, "FontType", 0)
	DllStructSetData($tlogfont, "Height", $lfheight)
	DllStructSetData($tlogfont, "Weight", $ifontweight)
	DllStructSetData($tlogfont, "Italic", $iitalic)
	DllStructSetData($tlogfont, "Underline", $iunderline)
	DllStructSetData($tlogfont, "Strikeout", $istrikethru)
	DllStructSetData($tlogfont, "FaceName", $sfontname)
	Local $aresult = DllCall("comdlg32.dll", "bool", "ChooseFontW", "ptr", DllStructGetPtr($tchoosefont))
	If @error Then Return SetError(@error, @extended, -1)
	If $aresult[0] = 0 Then Return SetError(-3, -3, -1)
	Local $fontname = DllStructGetData($tlogfont, "FaceName")
	If StringLen($fontname) = 0 AND StringLen($sfontname) > 0 Then $fontname = $sfontname
	If DllStructGetData($tlogfont, "Italic") Then $italic = 2
	If DllStructGetData($tlogfont, "Underline") Then $underline = 4
	If DllStructGetData($tlogfont, "Strikeout") Then $strikeout = 8
	Local $attributes = BitOR($italic, $underline, $strikeout)
	Local $size = DllStructGetData($tchoosefont, "PointSize") / 10
	Local $colorref = DllStructGetData($tchoosefont, "rgbColors")
	Local $weight = DllStructGetData($tlogfont, "Weight")
	Local $color_picked = Hex(String($colorref), 6)
	Return StringSplit($attributes & "," & $fontname & "," & $size & "," & $weight & "," & $colorref & "," & "0x" & $color_picked & "," & "0x" & StringMid($color_picked, 5, 2) & StringMid($color_picked, 3, 2) & StringMid($color_picked, 1, 2), ",")
EndFunc

Func _clipputfile($sfile, $sseparator = "|")
	Local Const $gmem_moveable = 2, $cf_hdrop = 15
	$sfile &= $sseparator & $sseparator
	Local $nglobmemsize = (StringLen($sfile) + 20)
	Local $aresult = DllCall("user32.dll", "bool", "OpenClipboard", "hwnd", 0)
	If @error OR $aresult[0] = 0 Then Return SetError(1, _winapi_getlasterror(), False)
	Local $ierror = 0, $ilasterror = 0
	$aresult = DllCall("user32.dll", "bool", "EmptyClipboard")
	If @error OR NOT $aresult[0] Then
		$ierror = 2
		$ilasterror = _winapi_getlasterror()
	Else
		$aresult = DllCall("kernel32.dll", "handle", "GlobalAlloc", "uint", $gmem_moveable, "ulong_ptr", $nglobmemsize)
		If @error OR NOT $aresult[0] Then
			$ierror = 3
			$ilasterror = _winapi_getlasterror()
		Else
			Local $hglobal = $aresult[0]
			$aresult = DllCall("kernel32.dll", "ptr", "GlobalLock", "handle", $hglobal)
			If @error OR NOT $aresult[0] Then
				$ierror = 4
				$ilasterror = _winapi_getlasterror()
			Else
				Local $hlock = $aresult[0]
				Local $dropfiles = DllStructCreate("dword;ptr;int;int;int;char[" & StringLen($sfile) + 1 & "]", $hlock)
				If @error Then Return SetError(5, 6, False)
				Local $tempstruct = DllStructCreate("dword;ptr;int;int;int")
				DllStructSetData($dropfiles, 1, DllStructGetSize($tempstruct))
				DllStructSetData($dropfiles, 2, 0)
				DllStructSetData($dropfiles, 3, 0)
				DllStructSetData($dropfiles, 4, 0)
				DllStructSetData($dropfiles, 5, 0)
				DllStructSetData($dropfiles, 6, $sfile)
				For $i = 1 To StringLen($sfile)
					If DllStructGetData($dropfiles, 6, $i) = $sseparator Then DllStructSetData($dropfiles, 6, Chr(0), $i)
				Next
				$aresult = DllCall("user32.dll", "handle", "SetClipboardData", "uint", $cf_hdrop, "handle", $hglobal)
				If @error OR NOT $aresult[0] Then
					$ierror = 6
					$ilasterror = _winapi_getlasterror()
				EndIf
				$aresult = DllCall("kernel32.dll", "bool", "GlobalUnlock", "handle", $hglobal)
				If (@error OR NOT $aresult[0]) AND NOT $ierror AND _winapi_getlasterror() Then
					$ierror = 8
					$ilasterror = _winapi_getlasterror()
				EndIf
			EndIf
			$aresult = DllCall("kernel32.dll", "ptr", "GlobalFree", "handle", $hglobal)
			If (@error OR NOT $aresult[0]) AND NOT $ierror Then
				$ierror = 9
				$ilasterror = _winapi_getlasterror()
			EndIf
		EndIf
	EndIf
	$aresult = DllCall("user32.dll", "bool", "CloseClipboard")
	If (@error OR NOT $aresult[0]) AND NOT $ierror Then Return SetError(7, _winapi_getlasterror(), False)
	If $ierror Then Return SetError($ierror, $ilasterror, False)
	Return True
EndFunc

Func _iif($ftest, $vtrueval, $vfalseval)
	If $ftest Then
		Return $vtrueval
	Else
		Return $vfalseval
	EndIf
EndFunc

Func _mousetrap($ileft = 0, $itop = 0, $iright = 0, $ibottom = 0)
	Local $aresult
	If @NumParams == 0 Then
		$aresult = DllCall("user32.dll", "bool", "ClipCursor", "ptr", 0)
		If @error OR NOT $aresult[0] Then Return SetError(1, _winapi_getlasterror(), False)
	Else
		If @NumParams == 2 Then
			$iright = $ileft + 1
			$ibottom = $itop + 1
		EndIf
		Local $trect = DllStructCreate($tagrect)
		DllStructSetData($trect, "Left", $ileft)
		DllStructSetData($trect, "Top", $itop)
		DllStructSetData($trect, "Right", $iright)
		DllStructSetData($trect, "Bottom", $ibottom)
		$aresult = DllCall("user32.dll", "bool", "ClipCursor", "ptr", DllStructGetPtr($trect))
		If @error OR NOT $aresult[0] Then Return SetError(2, _winapi_getlasterror(), False)
	EndIf
	Return True
EndFunc

Func _singleton($soccurencename, $iflag = 0)
	Local Const $error_already_exists = 183
	Local Const $security_descriptor_revision = 1
	Local $psecurityattributes = 0
	If BitAND($iflag, 2) Then
		Local $tsecuritydescriptor = DllStructCreate("dword[5]")
		Local $psecuritydescriptor = DllStructGetPtr($tsecuritydescriptor)
		Local $aret = DllCall("advapi32.dll", "bool", "InitializeSecurityDescriptor", "ptr", $psecuritydescriptor, "dword", $security_descriptor_revision)
		If @error Then Return SetError(@error, @extended, 0)
		If $aret[0] Then
			$aret = DllCall("advapi32.dll", "bool", "SetSecurityDescriptorDacl", "ptr", $psecuritydescriptor, "bool", 1, "ptr", 0, "bool", 0)
			If @error Then Return SetError(@error, @extended, 0)
			If $aret[0] Then
				Local $structsecurityattributes = DllStructCreate($tagsecurity_attributes)
				DllStructSetData($structsecurityattributes, 1, DllStructGetSize($structsecurityattributes))
				DllStructSetData($structsecurityattributes, 2, $psecuritydescriptor)
				DllStructSetData($structsecurityattributes, 3, 0)
				$psecurityattributes = DllStructGetPtr($structsecurityattributes)
			EndIf
		EndIf
	EndIf
	Local $handle = DllCall("kernel32.dll", "handle", "CreateMutexW", "ptr", $psecurityattributes, "bool", 1, "wstr", $soccurencename)
	If @error Then Return SetError(@error, @extended, 0)
	Local $lasterror = DllCall("kernel32.dll", "dword", "GetLastError")
	If @error Then Return SetError(@error, @extended, 0)
	If $lasterror[0] = $error_already_exists Then
		If BitAND($iflag, 1) Then
			Return SetError($lasterror[0], $lasterror[0], 0)
		Else
			Exit  - 1
		EndIf
	EndIf
	Return $handle[0]
EndFunc

Func _ispressed($shexkey, $vdll = "user32.dll")
	Local $a_r = DllCall($vdll, "short", "GetAsyncKeyState", "int", "0x" & $shexkey)
	If @error Then Return SetError(@error, @extended, False)
	Return BitAND($a_r[0], 32768) <> 0
EndFunc

Func _versioncompare($sversion1, $sversion2)
	If $sversion1 = $sversion2 Then Return 0
	Local $sep = "."
	If StringInStr($sversion1, $sep) = 0 Then $sep = ","
	Local $aversion1 = StringSplit($sversion1, $sep)
	Local $aversion2 = StringSplit($sversion2, $sep)
	If UBound($aversion1) <> UBound($aversion2) OR UBound($aversion1) = 0 Then
		SetExtended(1)
		If $sversion1 > $sversion2 Then
			Return 1
		ElseIf $sversion1 < $sversion2 Then
			Return  - 1
		EndIf
	Else
		For $i = 1 To UBound($aversion1) - 1
			If StringIsDigit($aversion1[$i]) AND StringIsDigit($aversion2[$i]) Then
				If Number($aversion1[$i]) > Number($aversion2[$i]) Then
					Return 1
				ElseIf Number($aversion1[$i]) < Number($aversion2[$i]) Then
					Return  - 1
				EndIf
			Else
				SetExtended(1)
				If $aversion1[$i] > $aversion2[$i] Then
					Return 1
				ElseIf $aversion1[$i] < $aversion2[$i] Then
					Return  - 1
				EndIf
			EndIf
		Next
	EndIf
	Return SetError(2, 0, 0)
EndFunc

Func __misc_getdc($hwnd)
	Local $aresult = DllCall("User32.dll", "handle", "GetDC", "hwnd", $hwnd)
	If @error OR NOT $aresult[0] Then Return SetError(1, _winapi_getlasterror(), 0)
	Return $aresult[0]
EndFunc

Func __misc_getdevicecaps($hdc, $iindex)
	Local $aresult = DllCall("GDI32.dll", "int", "GetDeviceCaps", "handle", $hdc, "int", $iindex)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aresult[0]
EndFunc

Func __misc_releasedc($hwnd, $hdc)
	Local $aresult = DllCall("User32.dll", "int", "ReleaseDC", "hwnd", $hwnd, "handle", $hdc)
	If @error Then Return SetError(@error, @extended, False)
	Return $aresult[0] <> 0
EndFunc

Global $dll = DllOpen("user32.dll")
$message = "Comparative Genomic Fingerprinting: Automated Scoring Module Ver 1.0" & @CRLF & "BioCalculator Automated Exporter" & @CRLF & @CRLF & "Designed by, Written by, Eduardo Taboada, Steven Mutschall and Christine Yu" & @CRLF & "Laboratory for Foodborne Zoonoses Lethbridge Unit" & @CRLF & "Public Heath Agency of Canada" & @CRLF & "Copyright July 2009"
SplashTextOn("CGF Auto Scoring - BioCalculator Auto Export", $message, 400, 100, -1, -1, 4, "", 9)
ControlSetText("Welcome", "", "Static1", $message)
For $i = 0 To 500 Step 1
	Sleep(1)
	If (_ispressed("01", $dll)) Then
		ExitLoop 
	EndIf
Next
SplashOff()
HotKeySet("{F1}", "aboutClicked")
Dim $platedirpaths[8]
Dim $highpath
Dim $lowpath
Dim $piddir
Dim $highthreshold
Dim $lowthreshold
Opt("GUIOnEventMode", 1)
$mainwindow = GUICreate("BioCalculator Automated Exporter", 475, 400)
GUISetOnEvent($gui_event_close, "CLOSEClicked")
GUICtrlCreateLabel("Plate Directories:", 5, 10)
GUICtrlCreateLabel("MP1:", 5, 35)
$mp1_input = GUICtrlCreateInput("", 35, 30, 380)
$mp1_browb = GUICtrlCreateButton("Browse", 35 + 385, 30 - 3, 50)
GUICtrlSetOnEvent($mp1_browb, "MP1BrowseClicked")
GUICtrlCreateLabel("MP2:", 5, 65)
$mp2_input = GUICtrlCreateInput("", 35, 60, 380)
$mp2_browb = GUICtrlCreateButton("Browse", 35 + 385, 60 - 3, 50)
GUICtrlSetOnEvent($mp2_browb, "MP2BrowseClicked")
GUICtrlCreateLabel("MP3:", 5, 95)
$mp3_input = GUICtrlCreateInput("", 35, 90, 380)
$mp3_browb = GUICtrlCreateButton("Browse", 35 + 385, 90 - 3, 50)
GUICtrlSetOnEvent($mp3_browb, "MP3BrowseClicked")
GUICtrlCreateLabel("MP4:", 5, 125)
$mp4_input = GUICtrlCreateInput("", 35, 120, 380)
$mp4_browb = GUICtrlCreateButton("Browse", 35 + 385, 120 - 3, 50)
GUICtrlSetOnEvent($mp4_browb, "MP4BrowseClicked")
GUICtrlCreateLabel("MP5:", 5, 155)
$mp5_input = GUICtrlCreateInput("", 35, 150, 380)
$mp5_browb = GUICtrlCreateButton("Browse", 35 + 385, 150 - 3, 50)
GUICtrlSetOnEvent($mp5_browb, "MP5BrowseClicked")
GUICtrlCreateLabel("MP6:", 5, 185)
$mp6_input = GUICtrlCreateInput("", 35, 180, 380)
$mp6_browb = GUICtrlCreateButton("Browse", 35 + 385, 180 - 3, 50)
GUICtrlSetOnEvent($mp6_browb, "MP6BrowseClicked")
GUICtrlCreateLabel("MP7:", 5, 215)
$mp7_input = GUICtrlCreateInput("", 35, 210, 380)
$mp7_browb = GUICtrlCreateButton("Browse", 35 + 385, 210 - 3, 50)
GUICtrlSetOnEvent($mp7_browb, "MP7BrowseClicked")
GUICtrlCreateLabel("MP8:", 5, 245)
$mp8_input = GUICtrlCreateInput("", 35, 240, 380)
$mp8_browb = GUICtrlCreateButton("Browse", 35 + 385, 240 - 3, 50)
GUICtrlSetOnEvent($mp8_browb, "MP8BrowseClicked")
GUICtrlCreateLabel("High Threshold:", 5, 275)
$ht_input = GUICtrlCreateInput("10.00", 85, 270, 50)
GUICtrlCreateLabel("%", 85 + 55, 275)
GUICtrlCreateLabel("Low Threshold:", 5, 305)
$lt_input = GUICtrlCreateInput("5.00", 85, 300, 50)
GUICtrlCreateLabel("%", 85 + 55, 305)
GUICtrlCreateLabel("Peak Calling Tables .pid Directory:", 170, 275)
$pid_input = GUICtrlCreateInput("", 170, 275 + 17, 245)
$pid_browb = GUICtrlCreateButton("Browse", 170 + 240 + 10, 275 + 13, 50)
GUICtrlSetOnEvent($pid_browb, "pidBrowseClicked")
GUICtrlCreateLabel("Output Directory:", 5, 275 + 13 + 50)
$out_input = GUICtrlCreateInput("", 90, 275 + 13 + 45, 325)
$out_browb = GUICtrlCreateButton("Browse", 170 + 240 + 10, 275 + 13 + 45, 50)
GUICtrlSetOnEvent($out_browb, "outBrowseClicked")
$ok_b = GUICtrlCreateButton("Run", 120, 365, 70)
GUICtrlSetOnEvent($ok_b, "runClicked")
$clear_b = GUICtrlCreateButton("Clear", 120 + 70 + 10, 365, 70)
GUICtrlSetOnEvent($clear_b, "clearClicked")
$close_b = GUICtrlCreateButton("Close", 120 + 70 + 70 + 20, 365, 70)
GUICtrlSetOnEvent($close_b, "CLOSEClicked")
$about_link = GUICtrlCreateLabel("Help", 5, 380, 50, 20)
If $about_link <> 0 Then
	GUICtrlSetFont($about_link, -1, -1, 4)
	GUICtrlSetColor($about_link, 255)
	GUICtrlSetCursor($about_link, 0)
EndIf
GUICtrlSetOnEvent($about_link, "aboutClicked")
GUISetState(@SW_SHOW, $mainwindow)
While 1
	Sleep(1000)
WEnd

Func aboutclicked()
	$aboutwindow = GUICreate("BC_AE -  Help", 300, 100)
	GUISetOnEvent($gui_event_close, "CLOSEClicked_about")
	$helpmsg = "At least one  of the MPX fields (where X is a integer between 1 and 8) and all other fields must be filled before hitting run. The computer cannot be used for anything else while the script is running. For additional help, see the README.html file included with the CGF_AutoScoring.zip software package."
	GUICtrlCreateLabel($helpmsg, 10, 10, 280, 90, $ss_left)
	GUISetState(@SW_SHOW, $aboutwindow)
EndFunc

Func autofillothermppaths($txt)
	$mp = GUICtrlRead($txt)
	$outputdir = GUICtrlRead($piddir)
	If ($outputdir == 0) Then
		$outputdir = StringLeft($mp, StringInStr($mp, "\", 0, -1) - 1)
		ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $outputdir = " & $outputdir & @CRLF)
		GUICtrlSetData($out_input, $outputdir)
	EndIf
	$i = Int(StringMid($mp, StringLen($mp)))
	$mp = StringLeft($mp, StringLen($mp) - 1)
	If (FileExists($mp & "1")) Then
		$path = GUICtrlRead($mp1_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp1_input, $mp & "1")
		EndIf
	EndIf
	If (FileExists($mp & "2")) Then
		$path = GUICtrlRead($mp2_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp2_input, $mp & "2")
		EndIf
	EndIf
	If (FileExists($mp & "3")) Then
		$path = GUICtrlRead($mp3_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp3_input, $mp & "3")
		EndIf
	EndIf
	If (FileExists($mp & "4")) Then
		$path = GUICtrlRead($mp4_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp4_input, $mp & "4")
		EndIf
	EndIf
	If (FileExists($mp & "5")) Then
		$path = GUICtrlRead($mp5_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp5_input, $mp & "5")
		EndIf
	EndIf
	If (FileExists($mp & "6")) Then
		$path = GUICtrlRead($mp6_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp6_input, $mp & "6")
		EndIf
	EndIf
	If (FileExists($mp & "7")) Then
		$path = GUICtrlRead($mp7_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp7_input, $mp & "7")
		EndIf
	EndIf
	If (FileExists($mp & "8")) Then
		$path = GUICtrlRead($mp8_input)
		If ($path == 0 OR $path == "") Then
			GUICtrlSetData($mp8_input, $mp & "8")
		EndIf
	EndIf
EndFunc

Func mp1browseclicked()
	$initdir = ""
	GUICtrlSetData($mp1_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp1_input)
EndFunc

Func mp2browseclicked()
	$initdir = GUICtrlRead($mp1_input)
	GUICtrlSetData($mp2_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp2_input)
EndFunc

Func mp3browseclicked()
	$initdir = GUICtrlRead($mp2_input)
	GUICtrlSetData($mp3_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp3_input)
EndFunc

Func mp4browseclicked()
	$initdir = GUICtrlRead($mp3_input)
	GUICtrlSetData($mp4_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp4_input)
EndFunc

Func mp5browseclicked()
	$initdir = GUICtrlRead($mp4_input)
	GUICtrlSetData($mp5_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp5_input)
EndFunc

Func mp6browseclicked()
	$initdir = GUICtrlRead($mp5_input)
	GUICtrlSetData($mp6_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp6_input)
EndFunc

Func mp7browseclicked()
	$initdir = GUICtrlRead($mp6_input)
	GUICtrlSetData($mp7_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp7_input)
EndFunc

Func mp8browseclicked()
	$initdir = GUICtrlRead($mp7_input)
	GUICtrlSetData($mp8_input, FileSelectFolder("Select a Folder", "", 4, $initdir))
	autofillothermppaths($mp8_input)
EndFunc

Func pidbrowseclicked()
	GUICtrlSetData($pid_input, FileSelectFolder("Select Folder with .pid files", ""))
EndFunc

Func outbrowseclicked()
	GUICtrlSetData($out_input, FileSelectFolder("Select an Output Folder", "", 1))
EndFunc

Func clearclicked()
	GUICtrlSetData($mp1_input, "")
	GUICtrlSetData($mp2_input, "")
	GUICtrlSetData($mp3_input, "")
	GUICtrlSetData($mp4_input, "")
	GUICtrlSetData($mp5_input, "")
	GUICtrlSetData($mp6_input, "")
	GUICtrlSetData($mp7_input, "")
	GUICtrlSetData($mp8_input, "")
	GUICtrlSetData($out_input, "")
	GUICtrlSetData($ht_input, "")
	GUICtrlSetData($lt_input, "")
	GUICtrlSetData($pid_input, "")
EndFunc

Func closeclicked()
	Exit 
EndFunc

Func closeclicked_about()
	GUISetState(@SW_HIDE)
EndFunc

Func runclicked()
	$platedirpaths[0] = GUICtrlRead($mp1_input)
	$platedirpaths[1] = GUICtrlRead($mp2_input)
	$platedirpaths[2] = GUICtrlRead($mp3_input)
	$platedirpaths[3] = GUICtrlRead($mp4_input)
	$platedirpaths[4] = GUICtrlRead($mp5_input)
	$platedirpaths[5] = GUICtrlRead($mp6_input)
	$platedirpaths[6] = GUICtrlRead($mp7_input)
	$platedirpaths[7] = GUICtrlRead($mp8_input)
	Dim $nummps
	$nummps = 0
	For $i = 0 To 7 Step 1
		If ($platedirpaths[$i] <> "") Then
			$nummps = $nummps + 1
		EndIf
	Next
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $numMPs = " & $nummps & @CRLF & ">Error code: " & @error & @CRLF)
	If (GUICtrlRead($pid_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please specify a directory with the .pid files before hitting Run.")
		Return 
	ElseIf (GUICtrlRead($ht_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please input a high threshold, eg 10.00,  before hitting Run.")
		Return 
	ElseIf (GUICtrlRead($lt_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please input a low threshold, eg 5.00, before hitting Run.")
		Return 
	ElseIf (GUICtrlRead($out_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please specify an output directory before hitting Run.")
		Return 
	ElseIf ($nummps = 0) Then
		MsgBox(48, "Error - Required Field Blank", "Please specify at least 1 multiplex folder before hitting Run.")
		Return 
	EndIf
	$highpath = GUICtrlRead($out_input) & "\High\"
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $highPath = " & $highpath & @CRLF & ">Error code: " & @error & @CRLF)
	$lowpath = GUICtrlRead($out_input) & "\Low\"
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $lowPath = " & $lowpath & @CRLF & ">Error code: " & @error & @CRLF)
	$piddir = GUICtrlRead($pid_input) & "\"
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $pidDir = " & $piddir & @CRLF & ">Error code: " & @error & @CRLF)
	$highthreshold = "{NUMPAD0}" & GUICtrlRead($ht_input) & "%"
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $highThreshold = " & $highthreshold & @CRLF & ">Error code: " & @error & @CRLF)
	$lowthreshold = "{NUMPAD0}" & GUICtrlRead($lt_input) & "%"
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $lowThreshold = " & $lowthreshold & @CRLF & ">Error code: " & @error & @CRLF)
	If (FileExists($highpath) = 0) Then
		DirCreate($highpath)
	EndIf
	If (FileExists($lowpath) = 0) Then
		DirCreate($lowpath)
	EndIf
	Run("C:\Program Files\QIAxcel BioCalculator\BioCalc.exe")
	WinWaitActive("BioCalculator")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : BioCalculator open and active" & @CRLF)
	WinWaitActive("Instrument Control", "", 2)
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for IC win" & @CRLF)
	If (WinActive("Instrument Control")) Then
		ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : Instrument Control window open and exists" & @CRLF)
		WinWaitActive("Instrument Control", "Adjust separation time:")
		WinClose("Instrument Control", "Create gel image window at start of acquisition")
		WinClose("Instrument Control", "Adjust separation time:")
	Else
		ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : Instrument Control did not open" & @CRLF)
	EndIf
	WinActivate("BioCalculator", "For Help, press F1")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : BioCalc activated" & @CRLF)
	WinWaitActive("BioCalculator")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for BioCalc to be the active win" & @CRLF)
	WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : BioCalc->File->Export" & @CRLF)
	Dim $buttontxt
	$buttontxt = ""
	For $mp = 0 To 7 Step 1
		If ($platedirpaths[$mp] <> "") Then
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $mp = " & $mp & @CRLF)
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $plateDirPaths[" & $mp & "] = " & $platedirpaths[$mp] & @CRLF)
			_reducememory()
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : Reduce mem has been run" & @CRLF)
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waiting for Plate Image & Result File Creator win" & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for Plate Image & Result File Creator win" & @CRLF)
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : setting up MP" & @CRLF)
			setupmp($mp + 1, $platedirpaths[$mp], 1)
			$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = " & $buttontxt & @CRLF)
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : MP " & ($mp + 1) & " has been setup" & @CRLF)
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waiting for Plate Image & Result File Creator win" & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for Plate Image & Result File Creator win" & @CRLF)
			$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = " & $buttontxt & @CRLF)
			$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			While $buttontxt == "Cancel"
				$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
				ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $buttonTxt = " & $buttontxt & @CRLF)
				Sleep(10)
			WEnd
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : got control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = " & $buttontxt & @CRLF)
		EndIf
	Next
	WinKill("Plate Image & Result File Creator")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : killed Plate Image & Result File Creator" & @CRLF)
	WinActivate("BioCalculator")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : BioCalculator activated" & @CRLF)
	WinWaitActive("BioCalculator")
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for BioCalculator" & @CRLF)
	$menutriggered = WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	While ($menutriggered == 0)
		ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : Menu not triggered! BioCalculator->File->Export" & @CRLF)
		Sleep(10)
		$menutriggered = WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	WEnd
	ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : BioCalculator->File->Export" & @CRLF)
	While (WinActive("Plate Image & Result File Creator") == 0)
		ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : Plate Image & Result File Creator window not active. Menu not triggered! BioCalculator->File->Export" & @CRLF)
		WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	WEnd
	For $mp = 0 To 7 Step 1
		If ($platedirpaths[$mp] <> "") Then
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $mp = " & $mp & @CRLF)
			_reducememory()
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : Reduce mem has been run" & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for Plate Image & Result File Creator win" & @CRLF)
			setupmp($mp + 1, $platedirpaths[$mp], 0)
			$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = " & $buttontxt & @CRLF)
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : MP " & ($mp + 1) & " has been setup" & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : waited for Plate Image & Result File Creator win" & @CRLF)
			$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = " & $buttontxt & @CRLF)
			$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			While $buttontxt == "Cancel"
				$buttontxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
				ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $buttonTxt = " & $buttontxt & @CRLF)
				Sleep(10)
			WEnd
		EndIf
	Next
	WinKill("Plate Image & Result File Creator")
	WinKill("BioCalculator")
	MsgBox(0, "Auotmated Export Completed", "Finished exporting data files for both high and low thresholds")
EndFunc

Func setupmp($mpnum, $platepath, $whichthreshold)
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:1]")
	WinWaitActive("Select folder")
	ControlSetText("Select folder", "", "[CLASS:Edit; INSTANCE:1]", $platepath)
	ControlClick("Select folder", "", "[CLASS:Button; INSTANCE:2]")
	WinWaitActive("Plate Image & Result File Creator")
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:3]")
	If ($whichthreshold = 1) Then
		ControlSetText("Plate Image & Result File Creator", "", "[CLASS:Edit; INSTANCE:4]", $highpath & "mp" & $mpnum & ".jpg")
	ElseIf ($whichthreshold = 0) Then
		ControlSetText("Plate Image & Result File Creator", "", "[CLASS:Edit; INSTANCE:4]", $lowpath & "mp" & $mpnum & ".jpg")
	EndIf
	ControlCommand("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:5]", "Check")
	ControlCommand("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:19]", "Check")
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:6]")
	WinWaitActive("Parameter setup")
	ControlClick("Parameter setup", "", "[CLASS:AfxWnd42; INSTANCE:1]", "", 2, 266, 29)
	If ($whichthreshold = 1) Then
		Send($highthreshold)
	ElseIf ($whichthreshold = 0) Then
		Send($lowthreshold)
	EndIf
	Send("{Enter}")
	ControlClick("Parameter setup", "", "[CLASS:Button; INSTANCE:14]")
	WinActivate("Plate Image & Result File Creator")
	WinWaitActive("Plate Image & Result File Creator")
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:20]")
	WinWaitActive("Binary Peak Calling - Peak Table")
	ControlClick("Binary Peak Calling - Peak Table", "", "[CLASS:Button; INSTANCE:5]")
	Send($piddir & "mp" & $mpnum & ".pid")
	ControlClick("Open", "", "[CLASS:Button; INSTANCE:2]")
	WinWaitActive("Binary Peak Calling - Peak Table")
	ControlClick("Binary Peak Calling - Peak Table", "", "[CLASS:Button; INSTANCE:8]")
	WinWaitActive("Plate Image & Result File Creator")
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
	$plateimageresult = WinActive("Plate Image & Result File Creator")
	While ($plateimageresult == 0)
		Sleep(1000)
		WinActivate("Plate Image & Result File Creator")
		$plateimageresult = WinActive("Plate Image & Result File Creator")
		ConsoleWrite("@@ Debug(" & @ScriptLineNumber & ") : $plateImageResult = " & $plateimageresult & @CRLF)
	WEnd
EndFunc

Func _reducememory($i_pid = -1)
	If $i_pid <> -1 Then
		Local $ai_handle = DllCall("kernel32.dll", "int", "OpenProcess", "int", 2035711, "int", False, "int", $i_pid)
		Local $ai_return = DllCall("psapi.dll", "int", "EmptyWorkingSet", "long", $ai_handle[0])
		DllCall("kernel32.dll", "int", "CloseHandle", "int", $ai_handle[0])
	Else
		Local $ai_return = DllCall("psapi.dll", "int", "EmptyWorkingSet", "long", -1)
	EndIf
	Return $ai_return[0]
EndFunc
