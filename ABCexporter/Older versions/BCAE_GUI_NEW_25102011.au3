#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=BCAE_GUI_NEW_25102011.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

	AutoIt Version: 3.3.0.0
	Author:         Christine Yu

	Script Function:
	BioCalculator Automated Exporter  with GUI. This script automates
	exporting 8 MP plates, each exported twice (high and low thresholds) for
	a total of 16 exports max

#ce ----------------------------------------------------------------------------
#include <GUIConstantsEx.au3>
#include <ButtonConstants.au3>
#include <StaticConstants.au3>
#include <Misc.au3>

Global $dll = DllOpen("user32.dll")

;Dispay the splash screen
$message = "Comparative Genomic Fingerprinting: Automated Scoring Module Ver 1.0" & @CRLF & "BioCalculator Automated Exporter" & @CRLF & @CRLF & "Designed by, Written by, Eduardo Taboada, Steven Mutschall and Christine Yu" & @CRLF & "Laboratory for Foodborne Zoonoses Lethbridge Unit" & @CRLF & "Public Heath Agency of Canada" & @CRLF & "Copyright July 2009"
SplashTextOn("CGF Auto Scoring - BioCalculator Auto Export", $message, 400, 100, -1, -1, 4, "", 9)
ControlSetText("Welcome", "", "Static1", $message)

For $i = 0 To 500 Step 1
	Sleep(1)
	If (_IsPressed("01", $dll)) Then
		ExitLoop
	EndIf
Next
SplashOff()


;set the help
HotKeySet("{F1}", "aboutClicked") ;named the help dialog about because it used to be the about dialog

;Global Vars
;declaring user input variables
Dim $plateDirPaths[8]
Dim $highPath
Dim $lowPath
Dim $pidDir
Dim $highThreshold
Dim $lowThreshold

;Create the GUI
Opt("GUIOnEventMode", 1)
$mainwindow = GUICreate("BioCalculator Automated Exporter", 475, 400)
GUISetOnEvent($GUI_EVENT_CLOSE, "CLOSEClicked")
GUICtrlCreateLabel("Plate Directories:", 5, 10)
GUICtrlCreateLabel("MP1:", 5, 35)
;$MP1_input = GUICtrlCreateInput("C:\Documents and Settings\user\Desktop\2011-10-24 CI4578-4587\Multiplex1", 35, 30, 380)
$MP1_input = GUICtrlCreateInput("", 35, 30, 380)
$MP1_browB = GUICtrlCreateButton("Browse", 35 + 385, 30 - 3, 50)
GUICtrlSetOnEvent($MP1_browB, "MP1BrowseClicked")
GUICtrlCreateLabel("MP2:", 5, 65)
$MP2_input = GUICtrlCreateInput("", 35, 60, 380)
$MP2_browB = GUICtrlCreateButton("Browse", 35 + 385, 60 - 3, 50)
GUICtrlSetOnEvent($MP2_browB, "MP2BrowseClicked")
GUICtrlCreateLabel("MP3:", 5, 95)
$MP3_input = GUICtrlCreateInput("", 35, 90, 380)
$MP3_browB = GUICtrlCreateButton("Browse", 35 + 385, 90 - 3, 50)
GUICtrlSetOnEvent($MP3_browB, "MP3BrowseClicked")
GUICtrlCreateLabel("MP4:", 5, 125)
$MP4_input = GUICtrlCreateInput("", 35, 120, 380)
$MP4_browB = GUICtrlCreateButton("Browse", 35 + 385, 120 - 3, 50)
GUICtrlSetOnEvent($MP4_browB, "MP4BrowseClicked")
GUICtrlCreateLabel("MP5:", 5, 155)
$MP5_input = GUICtrlCreateInput("", 35, 150, 380)
$MP5_browB = GUICtrlCreateButton("Browse", 35 + 385, 150 - 3, 50)
GUICtrlSetOnEvent($MP5_browB, "MP5BrowseClicked")
GUICtrlCreateLabel("MP6:", 5, 185)
$MP6_input = GUICtrlCreateInput("", 35, 180, 380)
$MP6_browB = GUICtrlCreateButton("Browse", 35 + 385, 180 - 3, 50)
GUICtrlSetOnEvent($MP6_browB, "MP6BrowseClicked")
GUICtrlCreateLabel("MP7:", 5, 215)
$MP7_input = GUICtrlCreateInput("", 35, 210, 380)
$MP7_browB = GUICtrlCreateButton("Browse", 35 + 385, 210 - 3, 50)
GUICtrlSetOnEvent($MP7_browB, "MP7BrowseClicked")
GUICtrlCreateLabel("MP8:", 5, 245)
$MP8_input = GUICtrlCreateInput("", 35, 240, 380)
$MP8_browB = GUICtrlCreateButton("Browse", 35 + 385, 240 - 3, 50)
GUICtrlSetOnEvent($MP8_browB, "MP8BrowseClicked")
GUICtrlCreateLabel("High Threshold:", 5, 275)
$ht_input = GUICtrlCreateInput("10.00", 85, 270, 50)
GUICtrlCreateLabel("%", 85 + 55, 275)
GUICtrlCreateLabel("Low Threshold:", 5, 305)
$lt_input = GUICtrlCreateInput("5.00", 85, 300, 50)
GUICtrlCreateLabel("%", 85 + 55, 305)
GUICtrlCreateLabel("Peak Calling Tables .pid Directory:", 170, 275)
;$pid_input = GUICtrlCreateInput("C:\Documents and Settings\user\Desktop\2010-07-09 peakCalling PIDs", 170, 275 + 17, 245)
$pid_input = GUICtrlCreateInput("", 170, 275 + 17, 245)
$pid_browB = GUICtrlCreateButton("Browse", 170 + 240 + 10, 275 + 13, 50)
GUICtrlSetOnEvent($pid_browB, "pidBrowseClicked")
GUICtrlCreateLabel("Output Directory:", 5, 275 + 13 + 50)
;$out_input = GUICtrlCreateInput("C:\Documents and Settings\user\Desktop\2011-10-24 CI4578-4587", 90, 275 + 13 + 45, 325)
$out_input = GUICtrlCreateInput("", 90, 275 + 13 + 45, 325)
$out_browB = GUICtrlCreateButton("Browse", 170 + 240 + 10, 275 + 13 + 45, 50)
GUICtrlSetOnEvent($out_browB, "outBrowseClicked")
$ok_B = GUICtrlCreateButton("Run", 120, 365, 70)
GUICtrlSetOnEvent($ok_B, "runClicked")
$clear_B = GUICtrlCreateButton("Clear", 120 + 70 + 10, 365, 70)
GUICtrlSetOnEvent($clear_B, "clearClicked")
$close_B = GUICtrlCreateButton("Close", 120 + 70 + 70 + 20, 365, 70)
GUICtrlSetOnEvent($close_B, "CLOSEClicked")
$about_link = GUICtrlCreateLabel("Help", 5, 380, 50, 20)
If $about_link <> 0 Then
	GUICtrlSetFont($about_link, -1, -1, 4)
	GUICtrlSetColor($about_link, 0x0000ff)
	GUICtrlSetCursor($about_link, 0)
EndIf
GUICtrlSetOnEvent($about_link, "aboutClicked")
GUISetState(@SW_SHOW, $mainwindow)
;put the script to sleep, wait for events
While 1
	Sleep(1000)
WEnd

;GUI Event Drivers
Func aboutClicked()
	$aboutWindow = GUICreate("BC_AE -  Help", 300, 100)
	GUISetOnEvent($GUI_EVENT_CLOSE, "CLOSEClicked_about")
	$helpMsg = "At least one  of the MPX fields (where X is a integer between 1 and 8) and all other fields must be filled before hitting run. The computer cannot be used for anything else while the script is running. For additional help, see the README.html file included with the CGF_AutoScoring.zip software package."
	;create the about window
	GUICtrlCreateLabel($helpMsg, 10, 10, 280, 90, $SS_LEFT)
	GUISetState(@SW_SHOW, $aboutWindow)
EndFunc   ;==>aboutClicked
;Auto fill the other multiplex directory paths in the control
;@param $txt textbox that has had its contents modified with a folder browser dialog
Func autoFillOtherMPPaths($txt)
	$mp = GUICtrlRead($txt)
	;get the base directory for the output folder if no output folder is specified
	$outputdir = GUICtrlRead($pidDir)
	If ($outputdir == 0) Then
		$outputdir = StringLeft($mp, StringInStr($mp, "\", 0, -1) - 1)
		ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $outputdir = ' & $outputdir & @CRLF); & '>Error code: ' & @error & @crlf) ;### Debug Console
		GUICtrlSetData($out_input, $outputdir)
	EndIf

	$i = Int(StringMid($mp, StringLen($mp)))
	$mp = StringLeft($mp, StringLen($mp) - 1)

	If (FileExists($mp & "1")) Then
		$path = GUICtrlRead($MP1_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP1_input, $mp & "1")
		EndIf
	EndIf

	If (FileExists($mp & "2")) Then
		$path = GUICtrlRead($MP2_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP2_input, $mp & "2")
		EndIf
	EndIf

	If (FileExists($mp & "3")) Then
		$path = GUICtrlRead($MP3_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP3_input, $mp & "3")
		EndIf
	EndIf

	If (FileExists($mp & "4")) Then
		$path = GUICtrlRead($MP4_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP4_input, $mp & "4")
		EndIf
	EndIf

	If (FileExists($mp & "5")) Then
		$path = GUICtrlRead($MP5_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP5_input, $mp & "5")
		EndIf
	EndIf

	If (FileExists($mp & "6")) Then
		$path = GUICtrlRead($MP6_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP6_input, $mp & "6")
		EndIf
	EndIf

	If (FileExists($mp & "7")) Then
		$path = GUICtrlRead($MP7_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP7_input, $mp & "7")
		EndIf
	EndIf

	If (FileExists($mp & "8")) Then
		$path = GUICtrlRead($MP8_input)
		If ($path == 0 Or $path == "") Then
			GUICtrlSetData($MP8_input, $mp & "8")
		EndIf
	EndIf
EndFunc   ;==>autoFillOtherMPPaths

Func MP1BrowseClicked()
	$initDir = ""
	GUICtrlSetData($MP1_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP1_input)
EndFunc   ;==>MP1BrowseClicked
Func MP2BrowseClicked()
	$initDir = GUICtrlRead($MP1_input)
	GUICtrlSetData($MP2_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP2_input)
EndFunc   ;==>MP2BrowseClicked
Func MP3BrowseClicked()
	$initDir = GUICtrlRead($MP2_input)
	GUICtrlSetData($MP3_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP3_input)
EndFunc   ;==>MP3BrowseClicked
Func MP4BrowseClicked()
	$initDir = GUICtrlRead($MP3_input)
	GUICtrlSetData($MP4_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP4_input)
EndFunc   ;==>MP4BrowseClicked
Func MP5BrowseClicked()
	$initDir = GUICtrlRead($MP4_input)
	GUICtrlSetData($MP5_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP5_input)
EndFunc   ;==>MP5BrowseClicked
Func MP6BrowseClicked()
	$initDir = GUICtrlRead($MP5_input)
	GUICtrlSetData($MP6_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP6_input)
EndFunc   ;==>MP6BrowseClicked
Func MP7BrowseClicked()
	$initDir = GUICtrlRead($MP6_input)
	GUICtrlSetData($MP7_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP7_input)
EndFunc   ;==>MP7BrowseClicked
Func MP8BrowseClicked()
	$initDir = GUICtrlRead($MP7_input)
	GUICtrlSetData($MP8_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
	autoFillOtherMPPaths($MP8_input)
EndFunc   ;==>MP8BrowseClicked
Func pidBrowseClicked()
	GUICtrlSetData($pid_input, FileSelectFolder("Select Folder with .pid files", ""))
EndFunc   ;==>pidBrowseClicked
Func outBrowseClicked()
	GUICtrlSetData($out_input, FileSelectFolder("Select an Output Folder", "", 1))
EndFunc   ;==>outBrowseClicked
Func clearClicked()
	GUICtrlSetData($MP1_input, "")
	GUICtrlSetData($MP2_input, "")
	GUICtrlSetData($MP3_input, "")
	GUICtrlSetData($MP4_input, "")
	GUICtrlSetData($MP5_input, "")
	GUICtrlSetData($MP6_input, "")
	GUICtrlSetData($MP7_input, "")
	GUICtrlSetData($MP8_input, "")
	GUICtrlSetData($out_input, "")
	GUICtrlSetData($ht_input, "")
	GUICtrlSetData($lt_input, "")
	GUICtrlSetData($pid_input, "")
EndFunc   ;==>clearClicked
Func CLOSEClicked()
	Exit
EndFunc   ;==>CLOSEClicked
Func CLOSEClicked_about()
	GUISetState(@SW_HIDE)
EndFunc   ;==>CLOSEClicked_about
Func runClicked()
	;assign user input to global vars
	$plateDirPaths[0] = GUICtrlRead($MP1_input)
	$plateDirPaths[1] = GUICtrlRead($MP2_input)
	$plateDirPaths[2] = GUICtrlRead($MP3_input)
	$plateDirPaths[3] = GUICtrlRead($MP4_input)
	$plateDirPaths[4] = GUICtrlRead($MP5_input)
	$plateDirPaths[5] = GUICtrlRead($MP6_input)
	$plateDirPaths[6] = GUICtrlRead($MP7_input)
	$plateDirPaths[7] = GUICtrlRead($MP8_input)

	;count and see how many multiplexes to run
	Dim $numMPs
	$numMPs = 0
	For $i = 0 To 7 Step 1
		If ($plateDirPaths[$i] <> "") Then
			$numMPs = $numMPs + 1
		EndIf
	Next
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $numMPs = ' & $numMPs & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console

	;display error msg if any input fields are empty
	If (GUICtrlRead($pid_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please specify a directory with the .pid files before hitting Run.")
		Return
	ElseIf (GUICtrlRead($ht_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please input a high threshold, eg 10.00,  before hitting Run.")
		Return
	ElseIf (GUICtrlRead($lt_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please input a low threshold, eg 5.00, before hitting Run.")
		Return
	ElseIf (GUICtrlRead($out_input) = "") Then
		MsgBox(48, "Error - Required Field Blank", "Please specify an output directory before hitting Run.")
		Return
	ElseIf ($numMPs = 0) Then
		MsgBox(48, "Error - Required Field Blank", "Please specify at least 1 multiplex folder before hitting Run.")
		Return
	EndIf
	$highPath = GUICtrlRead($out_input) & "\High\"
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $highPath = ' & $highPath & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
	$lowPath = GUICtrlRead($out_input) & "\Low\"
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $lowPath = ' & $lowPath & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
	$pidDir = GUICtrlRead($pid_input) & "\"
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $pidDir = ' & $pidDir & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
	$highThreshold = "{NUMPAD0}" & GUICtrlRead($ht_input) & "%"
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $highThreshold = ' & $highThreshold & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
	$lowThreshold = "{NUMPAD0}" & GUICtrlRead($lt_input) & "%"
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $lowThreshold = ' & $lowThreshold & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console

	;check if the above two paths exist, if not create them
	If (FileExists($highPath) = 0) Then
		DirCreate($highPath)
	EndIf
	If (FileExists($lowPath) = 0) Then
		DirCreate($lowPath)
	EndIf

	;run Biocalc, and bring up the export dialog
	Run("C:\Program Files\QIAxcel BioCalculator\BioCalc.exe")
	WinWaitActive("BioCalculator")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : BioCalculator open and active' & @CRLF)
	;wait 1 second for IC win to come up
	WinWaitActive("Instrument Control", "", 2)
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for IC win' & @CRLF)
	If (WinActive("Instrument Control")) Then
		ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : Instrument Control window open and exists' & @CRLF)
		WinWaitActive("Instrument Control", "Adjust separation time:")
		WinClose("Instrument Control", "Create gel image window at start of acquisition")
		WinClose("Instrument Control", "Adjust separation time:")
	Else
		ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : Instrument Control did not open' & @CRLF)
	EndIf
	WinActivate("BioCalculator", "For Help, press F1")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : BioCalc activated' & @CRLF)
	WinWaitActive("BioCalculator")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for BioCalc to be the active win' & @CRLF)
	WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : BioCalc->File->Export' & @CRLF)
	Dim $buttonTxt
	$buttonTxt = ""

	;start processing the multiplexes, high threshold first
	For $mp = 0 To 7 Step 1
		If ($plateDirPaths[$mp] <> "") Then
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $mp = ' & $mp & @CRLF)
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $plateDirPaths[' & $mp & '] = ' & $plateDirPaths[$mp] & @CRLF)
			_ReduceMemory()
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : Reduce mem has been run' & @CRLF)
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waiting for Plate Image & Result File Creator win' & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for Plate Image & Result File Creator win' & @CRLF)
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : setting up MP' & @CRLF)
			setUpMP($mp + 1, $plateDirPaths[$mp], 1)
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = ' & $buttonTxt & @CRLF)
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : MP ' & ($mp + 1) & ' has been setup' & @CRLF)
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waiting for Plate Image & Result File Creator win' & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for Plate Image & Result File Creator win' & @CRLF)
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = ' & $buttonTxt & @CRLF)
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			While $buttonTxt == "Cancel"
				$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
				ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $buttonTxt = ' & $buttonTxt & @CRLF)
				Sleep(10)
			WEnd
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : got control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = ' & $buttonTxt & @CRLF)
		EndIf
	Next
	;Close the biocalculator and then restart because of issues running mp8 and running out of memory
	WinKill("Plate Image & Result File Creator")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : killed Plate Image & Result File Creator' & @CRLF)
;~ 	WinKill("BioCalculator")
;~ 	ProgressOn("Please wait for Biocalculator to rerun", "Please Wait", "0 percent")
;~ 	For $i = 10 To 100 Step 10
;~ 		Sleep(1000)
;~ 		ProgressSet($i, $i & " percent")
;~ 	Next
;~ 	ProgressSet(100, "Now rerunning Qiaxcel Biocalcator", "Rerunning Biocalculator...")
;~ 	Sleep(500)
;~ 	ProgressOff()

;~ 	;rerun
;~ 	Run("C:\Program Files\QIAxcel BioCalculator\BioCalc.exe")
;~ 	WinWaitActive("Instrument Control", "Adjust separation time:")
;~ 	WinClose("Instrument Control", "Adjust separation time:")
	WinActivate("BioCalculator")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : BioCalculator activated' & @CRLF)
	WinWaitActive("BioCalculator")
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for BioCalculator' & @CRLF)
	$menuTriggered = WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	while ($menuTriggered == 0)
		ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : Menu not triggered! BioCalculator->File->Export' & @CRLF)
		Sleep(10)
		$menuTriggered = WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	WEnd
	ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : BioCalculator->File->Export' & @CRLF)
	while (WinActive("Plate Image & Result File Creator") == 0)
		ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : Plate Image & Result File Creator window not active. Menu not triggered! BioCalculator->File->Export' & @CRLF)
		WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
	WEnd

	;process the low threshold
	For $mp = 0 To 7 Step 1
		If ($plateDirPaths[$mp] <> "") Then
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $mp = ' & $mp & @CRLF)
			_ReduceMemory()
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : Reduce mem has been run' & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for Plate Image & Result File Creator win' & @CRLF)
			setUpMP($mp + 1, $plateDirPaths[$mp], 0)
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = ' & $buttonTxt & @CRLF)
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : MP ' & ($mp + 1) & ' has been setup' & @CRLF)
			WinWaitActive("Plate Image & Result File Creator")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : waited for Plate Image & Result File Creator win' & @CRLF)
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : getting control text for Plate Image & Result File Creator [CLASS:Button; INSTANCE:21] = ' & $buttonTxt & @CRLF)
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			While $buttonTxt == "Cancel"
				$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
				ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $buttonTxt = ' & $buttonTxt & @CRLF)
				Sleep(10)
			WEnd
		EndIf
	Next

	;Close because we're finished!
	WinKill("Plate Image & Result File Creator")
	WinKill("BioCalculator")
	MsgBox(0, "Auotmated Export Completed", "Finished exporting data files for both high and low thresholds")
EndFunc   ;==>runClicked

;Processing code. Sets everything up as the QiAxcel Report Creator.xls VBA script needs.
;@param $mpNum the multiplex number to proces
;@param $platePath the path where the Qiaxcel Raw data files are stored for this multiplex plate
;@param whichThreshold is a bool 0 is low threshold, 1 is high threshold
Func setUpMP($mpNum, $platePath, $whichThreshold)
	;start setting up user inputs
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:1]")
	WinWaitActive("Select folder")
	ControlSetText("Select folder", "", "[CLASS:Edit; INSTANCE:1]", $platePath)
	ControlClick("Select folder", "", "[CLASS:Button; INSTANCE:2]")
	WinWaitActive("Plate Image & Result File Creator")
	;click Select All
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:3]")
	If ($whichThreshold = 1) Then
		ControlSetText("Plate Image & Result File Creator", "", "[CLASS:Edit; INSTANCE:4]", $highPath & "mp" & $mpNum & ".jpg")
	ElseIf ($whichThreshold = 0) Then
		ControlSetText("Plate Image & Result File Creator", "", "[CLASS:Edit; INSTANCE:4]", $lowPath & "mp" & $mpNum & ".jpg")
	EndIf

	;check use these integration parameters
	;ControlSend("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:5]", "-")
	ControlCommand("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:5]", "Check")
	;check enable binary scoring
	ControlCommand("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:19]", "Check")
	;click Params button
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:6]")
	;click on pos threshold
	WinWaitActive("Parameter setup")
	ControlClick("Parameter setup", "", "[CLASS:AfxWnd42; INSTANCE:1]", "", 2, 266, 29)
	If ($whichThreshold = 1) Then
		Send($highThreshold)
	ElseIf ($whichThreshold = 0) Then
		Send($lowThreshold)
	EndIf
	Send("{Enter}")
	;click ok
	ControlClick("Parameter setup", "", "[CLASS:Button; INSTANCE:14]")
	WinActivate("Plate Image & Result File Creator")
	WinWaitActive("Plate Image & Result File Creator")
	;click Table
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:20]")
	;click open
	WinWaitActive("Binary Peak Calling - Peak Table")
	ControlClick("Binary Peak Calling - Peak Table", "", "[CLASS:Button; INSTANCE:5]")
	;input path
	Send($pidDir & "mp" & $mpNum & ".pid")
	;click open
	ControlClick("Open", "", "[CLASS:Button; INSTANCE:2]")
	WinWaitActive("Binary Peak Calling - Peak Table")
	;Click OK
	ControlClick("Binary Peak Calling - Peak Table", "", "[CLASS:Button; INSTANCE:8]")
	;Click Process
	WinWaitActive("Plate Image & Result File Creator")
	ControlClick("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")

	;if any error messages pop up, wait until the user deals with it
	;Sleep(2000)
	$plateImageResult = WinActive("Plate Image & Result File Creator")
	While ($plateImageResult == 0)
		Sleep(1000)
		WinActivate("Plate Image & Result File Creator")
		$plateImageResult = WinActive("Plate Image & Result File Creator")
		ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $plateImageResult = ' & $plateImageResult & @CRLF)
	WEnd
EndFunc   ;==>setUpMP

; Reduce memory usage
; Author wOuter ( mostly )
Func _ReduceMemory($i_PID = -1)
	If $i_PID <> -1 Then
		Local $ai_Handle = DllCall("kernel32.dll", 'int', 'OpenProcess', 'int', 0x1f0fff, 'int', False, 'int', $i_PID)
		Local $ai_Return = DllCall("psapi.dll", 'int', 'EmptyWorkingSet', 'long', $ai_Handle[0])
		DllCall('kernel32.dll', 'int', 'CloseHandle', 'int', $ai_Handle[0])
	Else
		Local $ai_Return = DllCall("psapi.dll", 'int', 'EmptyWorkingSet', 'long', -1)
	EndIf
	Return $ai_Return[0]
EndFunc   ;==>_ReduceMemory

