#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.0.0
 Author:         Christine Yu

 Script Function:
	BioCalculator Automated Exporter  with GUI. This script automates
	exporting 8 MP plates, each exported twice (high and low thresholds) for
	a total of 16 exports max

#ce ----------------------------------------------------------------------------
#include <GUIConstantsEx.au3>
#include <ButtonConstants.au3>
#include <StaticConstants.au3>

;Dispay the splash screen
$message = "Comparative Genomic Fingerprinting: Automated Scoring Module Ver 1.0" & @CRLF & "BioCalculator Automated Exporter" & @CRLF & @CRLF & "Designed by, Written by, Eduardo Taboada, Steven Mutschall and Christine Yu" & @CRLF & "Laboratory for Foodborne Zoonoses Lethbridge Unit" & @CRLF & "Public Heath Agency of Canada" & @CRLF & "Copyright July 2009"
SplashTextOn("CGF Auto Scoring - BioCalculator Auto Export", $message, 400, 100, -1, -1, 4, "", 9)
    ControlSetText("Welcome", "", "Static1", $message)
    sleep(3500)
	SplashOff()


;set the help
HotKeySet("{F1}", "aboutClicked") ;named the help dialog about because it used to be the about dialog

;Global Vars
;declaring user input variables
Dim $plateDirPaths[8]
Dim $highPath
Dim $lowPath
Dim $pidDir
Dim $highThreshold
Dim $lowThreshold

;Create the GUI
Opt("GUIOnEventMode", 1)

$mainwindow = GUICreate("BioCalculator Automated Exporter", 475, 400)
GUISetOnEvent($GUI_EVENT_CLOSE, "CLOSEClicked")

GUICtrlCreateLabel("Plate Directories:", 5, 10)

GUICtrlCreateLabel("MP1:", 5, 35)
$MP1_input = GUICtrlCreateInput("", 35, 30, 380)
$MP1_browB = GUICtrlCreateButton("Browse", 35 + 385, 30 - 3, 50)
GUICtrlSetOnEvent($MP1_browB, "MP1BrowseClicked")

GUICtrlCreateLabel("MP2:", 5, 65)
$MP2_input = GUICtrlCreateInput("", 35, 60, 380)
$MP2_browB = GUICtrlCreateButton("Browse", 35 + 385, 60 - 3, 50)
GUICtrlSetOnEvent($MP2_browB, "MP2BrowseClicked")

GUICtrlCreateLabel("MP3:", 5, 95)
$MP3_input = GUICtrlCreateInput("", 35, 90, 380)
$MP3_browB = GUICtrlCreateButton("Browse", 35 + 385, 90 - 3, 50)
GUICtrlSetOnEvent($MP3_browB, "MP3BrowseClicked")

GUICtrlCreateLabel("MP4:", 5, 125)
$MP4_input = GUICtrlCreateInput("", 35, 120, 380)
$MP4_browB = GUICtrlCreateButton("Browse", 35 + 385, 120 - 3, 50)
GUICtrlSetOnEvent($MP4_browB, "MP4BrowseClicked")

GUICtrlCreateLabel("MP5:", 5, 155)
$MP5_input = GUICtrlCreateInput("", 35, 150, 380)
$MP5_browB = GUICtrlCreateButton("Browse", 35 + 385, 150 - 3, 50)
GUICtrlSetOnEvent($MP5_browB, "MP5BrowseClicked")

GUICtrlCreateLabel("MP6:", 5, 185)
$MP6_input = GUICtrlCreateInput("", 35, 180, 380)
$MP6_browB = GUICtrlCreateButton("Browse", 35 + 385, 180 - 3, 50)
GUICtrlSetOnEvent($MP6_browB, "MP6BrowseClicked")

GUICtrlCreateLabel("MP7:", 5, 215)
$MP7_input = GUICtrlCreateInput("", 35, 210, 380)
$MP7_browB = GUICtrlCreateButton("Browse", 35 + 385,210 - 3, 50)
GUICtrlSetOnEvent($MP7_browB, "MP7BrowseClicked")

GUICtrlCreateLabel("MP8:", 5, 245)
$MP8_input = GUICtrlCreateInput("", 35, 240, 380)
$MP8_browB = GUICtrlCreateButton("Browse", 35 + 385, 240 - 3, 50)
GUICtrlSetOnEvent($MP8_browB, "MP8BrowseClicked")

GUICtrlCreateLabel("High Threshold:", 5, 275)
$ht_input = GUICtrlCreateInput("10.00", 85, 270, 50)
GUICtrlCreateLabel("%", 85+ 55, 275)
GUICtrlCreateLabel("Low Threshold:", 5, 305)
$lt_input = GUICtrlCreateInput("5.00", 85, 300, 50)
GUICtrlCreateLabel("%", 85+ 55, 305)

GUICtrlCreateLabel("Peak Calling Tables .pid Directory:", 170, 275)
$pid_input = GUICtrlCreateInput("", 170, 275 + 17, 245)
$pid_browB = GUICtrlCreateButton("Browse", 170 + 240 + 10, 275 + 13, 50)
GUICtrlSetOnEvent($pid_browB, "pidBrowseClicked")

GUICtrlCreateLabel("Output Directory:", 5, 275 + 13 + 50)
$out_input = GUICtrlCreateInput("", 90, 275 + 13 + 45, 325)
$out_browB = GUICtrlCreateButton("Browse", 170 + 240 + 10, 275 + 13 + 45, 50)
GUICtrlSetOnEvent($out_browB, "outBrowseClicked")

$ok_B = GUICtrlCreateButton("Run", 120, 365, 70)
GUICtrlSetOnEvent($ok_B, "runClicked")

$clear_B = GUICtrlCreateButton("Clear", 120 + 70 + 10, 365, 70)
GUICtrlSetOnEvent($clear_B, "clearClicked")

$close_B = GUICtrlCreateButton("Close", 120 + 70 + 70 + 20, 365, 70)
GUICtrlSetonEvent($close_B, "CLOSEClicked")

$about_link = GUICtrlCreateLabel("Help", 5, 380, 50, 20)
   If $about_link <> 0 Then
      GUICtrlSetFont($about_link, -1, -1, 4)
      GUICtrlSetColor($about_link, 0x0000ff)
      GUICtrlSetCursor($about_link, 0)
	EndIf
GUICtrlSetonEvent($about_link, "aboutClicked")


GUISetState(@SW_SHOW, $mainwindow)
;put the script to sleep, wait for events

While 1
	Sleep(1000)
Wend

;GUI Event Drivers
Func aboutClicked()
$aboutWindow = GUICreate("BC_AE -  Help", 300, 100)
GUISetOnEvent($GUI_EVENT_CLOSE, "CLOSEClicked_about")
$helpMsg = "At least one  of the MPX fields (where X is a integer between 1 and 8) and all other fields must be filled before hitting run. The computer cannot be used for anything else while the script is running. For additional help, see the README.html file included with the CGF_AutoScoring.zip software package."
;create the about window
GUICTrlCreateLabel($helpMsg, 10, 10, 280, 90, $SS_LEFT)

GUISetState(@SW_SHOW, $aboutWindow)
EndFunc

Func MP1BrowseClicked()
	$initDir = ""
	GUICtrlSetData( $MP1_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func MP2BrowseClicked()
	$initDir = GUICtrlRead($MP1_input)
	GUICtrlSetData( $MP2_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func MP3BrowseClicked()
	$initDir = GUICtrlRead($MP2_input)
	GUICtrlSetData( $MP3_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func MP4BrowseClicked()
	$initDir = GUICtrlRead($MP3_input)
	GUICtrlSetData( $MP4_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func MP5BrowseClicked()
	$initDir = GUICtrlRead($MP4_input)
	GUICtrlSetData( $MP5_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc


Func MP6BrowseClicked()
	$initDir = GUICtrlRead($MP5_input)
	GUICtrlSetData( $MP6_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func MP7BrowseClicked()
	$initDir = GUICtrlRead($MP6_input)
	GUICtrlSetData( $MP7_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func MP8BrowseClicked()
	$initDir = GUICtrlRead($MP7_input)
	GUICtrlSetData( $MP8_input, FileSelectFolder("Select a Folder", "", 4, $initDir))
EndFunc

Func pidBrowseClicked()
	GUICtrlSetData( $pid_input, FileSelectFolder("Select Folder with .pid files", ""))
EndFunc

Func outBrowseClicked()
	GUICtrlSetData( $out_input, FileSelectFolder("Select an Output Folder", "", 1))
EndFunc

Func clearClicked()
	GUICtrlSetData($MP1_input, "")
	GUICtrlSetData($MP2_input, "")
	GUICtrlSetData($MP3_input, "")
	GUICtrlSetData($MP4_input, "")
	GUICtrlSetData($MP5_input, "")
	GUICtrlSetData($MP6_input, "")
	GUICtrlSetData($MP7_input, "")
	GUICtrlSetData($MP8_input, "")
	GUICtrlSetData($out_input, "")
	GUICtrlSetData($ht_input, "")
	GUICtrlSetData($lt_input, "")
	GUICtrlSetData($pid_input, "")

EndFunc

Func CLOSEClicked()
	Exit
EndFunc

Func CLOSEClicked_about()
	GUISetState(@SW_HIDE)
EndFunc

Func runClicked()
;assign user input to global vars
$plateDirPaths[0] = GUICtrlRead($MP1_input)
$plateDirPaths[1] = GUICtrlRead($MP2_input)
$plateDirPaths[2] = GUICtrlRead($MP3_input)
$plateDirPaths[3] = GUICtrlRead($MP4_input)
$plateDirPaths[4] = GUICtrlRead($MP5_input)
$plateDirPaths[5] = GUICtrlRead($MP6_input)
$plateDirPaths[6] = GUICtrlRead($MP7_input)
$plateDirPaths[7] = GUICtrlRead($MP8_input)

;count and see how many multiplexes to run
Dim $numMPs
$numMPs = 0
for $i = 0 to 7 step 1
	if($plateDirPaths[$i] <> "") Then
		$numMPs = $numMPs + 1
	EndIf
Next

;display error msg if any input fields are empty
If(GUICtrlRead($pid_input) = "") Then
	MsgBox(48, "Error - Required Field Blank", "Please specify a directory with the .pid files before hitting Run.")
	Return
ElseIf(GUICtrlRead($ht_input) = "") Then
	MsgBox(48,  "Error - Required Field Blank", "Please input a high threshold, eg 10.00,  before hitting Run.")
	Return
ElseIf(GUICtrlRead($lt_input) = "") Then
	MsgBox(48,  "Error - Required Field Blank", "Please input a low threshold, eg 5.00, before hitting Run.")
	Return
Elseif(GUICtrlRead($out_input) = "") Then
	MsgBox(48,  "Error - Required Field Blank", "Please specify an output directory before hitting Run.")
	Return
Elseif($numMPs = 0) Then
	MsgBox(48,  "Error - Required Field Blank", "Please specify at least 1 multiplex folder before hitting Run.")
	Return
EndIf

$highPath = GUICtrlRead($out_input) & "\High\"
$lowPath = GUICtrlRead($out_input) & "\Low\"

$pidDir = GUICtrlRead($pid_input) & "\"

$highThreshold = "{NUMPAD0}" & GUICtrlRead($ht_input) & "%"
$lowThreshold = "{NUMPAD0}" & GUICtrlRead($lt_input) & "%"

;check if the above two paths exist, if not create them
If(FileExists($highPath) = 0) Then
	DirCreate($highPath)
EndIf
If(FileExists($lowPath) = 0) Then
	DirCreate($lowPath)
EndIf

;run Biocalc, and bring up the export dialog
;MsgBox ( 0, "test", "1")
Run("C:\Program Files\QIAxcel BioCalculator\BioCalc.exe")
;MsgBox ( 0, "test", "2")
;WinWaitActive("Instrument Control", "Create gel image window at start of acquisition")
;WinWaitActive("Instrument Control", "Adjust separation time:")
;MsgBox ( 0, "test", "3")
;WinClose("Instrument Control", "Create gel image window at start of acquisition")
;WinClose("Instrument Control", "Adjust separation time:")
;MsgBox ( 0, "test", "4")



;WinWaitActive("BioCalculator", "For Help, press F1")
WinWaitActive("BioCalculator","For Help, press F1")
;MsgBox ( 0, "test", "5")

WinMenuSelectItem("BioCalculator", "", "&File", "&Export")
;MsgBox ( 0, "test", "6")
Dim $buttonTxt
$buttonTxt = ""

;start processing the multiplexes, high threshold first
For $mp = 0 to 7 Step 1

	if($plateDirPaths[$mp] <> "") Then

		_ReduceMemory()
		MsgBox ( 0, "test", "6")
		WinWaitActive("Plate Image & Result File Creator")
		setUpMP($mp + 1, $plateDirPaths[$mp], 1)
		MsgBox ( 0, "test", "7")

		WinWaitActive("Plate Image & Result File Creator")
		$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")

		While $buttonTxt <> "Process"
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			Sleep(100)
		Wend

	Endif
Next
;Close the biocalculator and then restart because of issues running mp8 and running out of memory
WinKill("Plate Image & Result File Creator")
WinKill("BioCalculator")

ProgressOn("Please wait for Biocalculator to rerun", "Please Wait", "0 percent")
For $i = 10 to 100 step 10
    sleep(1000)
    ProgressSet( $i, $i & " percent")
Next
ProgressSet(100 , "Now rerunning Qiaxcel Biocalcator", "Rerunning Biocalculator...")
sleep(500)
ProgressOff()

;rerun
Run("C:\Program Files\QIAxcel BioCalculator\BioCalc.exe")
;WinWaitActive("Instrument Control", "Adjust separation time:")
;WinClose("Instrument Control", "Adjust separation time:")
;WinActivate("BioCalculator")

WinWaitActive("BioCalculator","For Help, press F1")

WinMenuSelectItem("BioCalculator", "", "&File", "&Export")

;process the low threshold
For $mp = 0 to 7 Step 1

	If($plateDirPaths[$mp] <> "") Then
		_ReduceMemory()

		WinWaitActive("Plate Image & Result File Creator")
		setUpMP($mp + 1, $plateDirPaths[$mp], 0)
		MsgBox ( 0, "test", "1") ;added
		WinWaitActive("Plate Image & Result File Creator")
		$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")

		While $buttonTxt <> "Process"
			$buttonTxt = ControlGetText("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:21]")
			Sleep(100)
		Wend
	EndIf
Next

;Close because we're finished!
WinKill("Plate Image & Result File Creator")
WinKill("BioCalculator")
MsgBox(0, "Auotmated Export Completed", "Finished exporting data files for both high and low thresholds")

EndFunc

;Processing code. Sets everything up as the QiAxcel Report Creator.xls VBA script needs.
;@param $mpNum the multiplex number to proces
;@param $platePath the path where the Qiaxcel Raw data files are stored for this multiplex plate
;@param whichThreshold is a bool 0 is low threshold, 1 is high threshold
Func setUpMP($mpNum, $platePath, $whichThreshold)
;start setting up user inputs
ControlClick("Plate Image & Result File Creator", "",  "[CLASS:Button; INSTANCE:1]")
WinWaitActive("Select folder")
ControlSetText("Select folder", "", "[CLASS:Edit; INSTANCE:1]", $platePath)
ControlClick("Select folder", "",  "[CLASS:Button; INSTANCE:2]")

;click Select All
WinWaitActive("Plate Image & Result File Creator")
ControlClick("Plate Image & Result File Creator", "Deselect All",  "Button3")

WinWaitActive("Plate Image & Result File Creator")
if($whichThreshold = 1) Then
	ControlSetText("Plate Image & Result File Creator", "", "[CLASS:Edit; INSTANCE:4]", $highPath & "mp" & $mpNum & ".jpg")
Elseif($whichThreshold = 0) Then
	ControlSetText("Plate Image & Result File Creator", "", "[CLASS:Edit; INSTANCE:4]", $lowPath & "mp" & $mpNum & ".jpg")
Endif

;check use these integration parameters
;ControlSend("Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:5]", "-")
ControlCommand ( "Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:5]", "Check")
;check enable binary scoring
ControlCommand ( "Plate Image & Result File Creator", "", "[CLASS:Button; INSTANCE:19]", "Check")
;click Params button
ControlClick("Plate Image & Result File Creator", "",  "[CLASS:Button; INSTANCE:6]")
;click on pos threshold
WinWaitActive("Parameter setup")
ControlClick("Parameter setup", "", "[CLASS:AfxWnd42; INSTANCE:1]", "", 2, 266, 29)
if($whichThreshold = 1) Then
	Send($highThreshold)
Elseif($whichThreshold = 0) Then
	Send($lowThreshold)
EndIf
Send("{Enter}")
;click ok
ControlClick("Parameter setup", "", "[CLASS:Button; INSTANCE:14]")
WinActivate("Plate Image & Result File Creator")
WinWaitActive("Plate Image & Result File Creator")
;click Table
ControlClick("Plate Image & Result File Creator", "",  "[CLASS:Button; INSTANCE:20]")
;click open
WinWaitActive("Binary Peak Calling - Peak Table")
ControlClick("Binary Peak Calling - Peak Table", "",  "[CLASS:Button; INSTANCE:5]")
;input path
Send($pidDir & "mp" & $mpNum & ".pid")
;click open
ControlClick("Open", "",  "[CLASS:Button; INSTANCE:2]")
WinWaitActive("Binary Peak Calling - Peak Table")
;Click OK
ControlClick("Binary Peak Calling - Peak Table", "",  "[CLASS:Button; INSTANCE:8]")
;Click Process
WinWaitActive("Plate Image & Result File Creator")
ControlClick("Plate Image & Result File Creator", "",  "[CLASS:Button; INSTANCE:21]")

;if any error messages pop up, wait until the user deals with it
Sleep(2000)
While (WinActive("Plate Image & Result File Creator") <> 1)
	Sleep(1000)
Wend
EndFunc

; Reduce memory usage
; Author wOuter ( mostly )

Func _ReduceMemory($i_PID = -1)

    If $i_PID <> -1 Then
        Local $ai_Handle = DllCall("kernel32.dll", 'int', 'OpenProcess', 'int', 0x1f0fff, 'int', False, 'int', $i_PID)
        Local $ai_Return = DllCall("psapi.dll", 'int', 'EmptyWorkingSet', 'long', $ai_Handle[0])
        DllCall('kernel32.dll', 'int', 'CloseHandle', 'int', $ai_Handle[0])
    Else
        Local $ai_Return = DllCall("psapi.dll", 'int', 'EmptyWorkingSet', 'long', -1)
    EndIf

    Return $ai_Return[0]
EndFunc;==> _ReduceMemory()

